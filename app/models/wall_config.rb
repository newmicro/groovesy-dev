class WallConfig
  attr_accessor :pinning, :on_your_page
  def initialize(fields)
    @pinning = fields[:pinning]
    @on_your_page = fields[:on_your_page]
  end
end