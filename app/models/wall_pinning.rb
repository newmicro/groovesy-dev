class WallPinning < ActiveRecord::Base
  belongs_to :user
  belongs_to :pinned_user, :foreign_key => "pinned_user_id", :class_name => "User"

  def self.next_slot(uid)
    WallPinning.where(:user_id => uid).maximum(:slot) + 1
  end

  def self.reorg_slots(wp)
    slot_nbr = wp.slot
    update_wps = wp.user.wall_pinnings.where("slot > #{slot_nbr}")
    WallPinning.update_all("slot = slot - 1", ["user_id = ? AND id IN (?)", wp.user_id, update_wps ])
  end
end