class Notification < ActiveRecord::Base
  belongs_to :user
  belongs_to :wall_post, :foreign_key => "notifiable_id"
  belongs_to :wall_post_comment, :foreign_key => "notifiable_id"
  belongs_to :friend_user, :foreign_key => "notifiable_id"
  belongs_to :wall_post_like, :foreign_key => "notifiable_id"
end