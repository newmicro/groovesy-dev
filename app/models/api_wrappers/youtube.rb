class ApiWrappers::Youtube
  require 'typhoeus'
  require 'json'

  def self.get_track_details(url)
    id = UrlParsers::Youtube.get_id(url)

    hydra = Typhoeus::Hydra.new
    request = Typhoeus::Request.new("https://gdata.youtube.com/feeds/api/videos/#{id}?v=2", :method => :get, :timeout => Constants::ApiTimeoutSeconds * 1000)
    hydra.queue(request)
    hydra.run
    response = request.response

    resp = Hash.from_xml(response.body)

    if resp.has_key?("entry") && resp["entry"].has_key?("author") && resp["entry"]["author"].has_key?("name")
      artist = resp['entry']['author']['name']
    end

    if resp.has_key?("entry") && resp["entry"].has_key?("group") && resp["entry"]["group"].has_key?("title")
      title = resp['entry']['group']['title']
    end

    if resp.has_key?("entry") && resp["entry"].has_key?("group") && resp["entry"]["group"].has_key?("description")
      description = resp['entry']['group']['description']
    end

    if resp.has_key?("entry") && resp["entry"].has_key?("content") && resp["entry"]["content"].has_key?("src")
      perma_link = resp['entry']['content']['src']
    end

    if artist.present? && title.present?
      return {:artist => artist, :title => title, :description => description, :provider => "Youtube", :thumbnail_url => "http://img.youtube.com/vi/#{id}/0.jpg", :perma_link => perma_link }
    end
    return {:artist => "", :title => "", :description => "", :provider => "Youtube", :thumbnail_url => "", :perma_link => "" }
  end

  def self.get_thumbnail_url(url)
    "http://img.youtube.com/vi/#{UrlParsers::Youtube.get_id(url)}/0.jpg"
  end
end