class ApiWrappers::Mixcloud
  require 'typhoeus'
  require 'json'

  def self.get_api_url(url)
    if !url.index("www.mixcloud.com").nil?
      key = "www.mixcloud.com"
    else
      key = "mixcloud.com"
    end
    url.gsub(key, "api.mixcloud.com")
  end

  def self.get_track_details(url)
    api = ApiWrappers::Mixcloud.get_api_url(url)

    hydra = Typhoeus::Hydra.new
    request = Typhoeus::Request.new(api, :method => :get, :timeout => Constants::ApiTimeoutSeconds * 1000)
    hydra.queue(request)
    hydra.run

    response = request.response
    resp = ActiveSupport::JSON.decode(response.body)

    title = resp["name"] if resp.has_key?("name")
    description = resp["description"] if resp.has_key?("description")
    artist = resp["user"]["name"] if resp.has_key?("user") && resp['user'].has_key?("name")
    thumbnail_url = resp["pictures"]["large"]
    perma_link = resp["url"]

    if artist.present? && title.present?
      return {:artist => artist, :title => title, :description => description, :provider => "Mixcloud", :thumbnail_url => thumbnail_url, :perma_link => perma_link }
    end
    return {:artist => "", :title => "", :description => "", :provider => "Mixcloud", :thumbnail_url => "", :perma_link => "" }
  end

  def self.get_thumbnail_url(url)
    api = ApiWrappers::Mixcloud.get_api_url(url)

    hydra = Typhoeus::Hydra.new
    request = Typhoeus::Request.new(api, :method => :get, :timeout => Constants::ApiTimeoutSeconds * 1000)
    hydra.queue(request)
    hydra.run

    response = request.response
    resp = ActiveSupport::JSON.decode(response.body)

    return resp["pictures"]["large"]
  end
end