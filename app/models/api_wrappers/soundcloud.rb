class ApiWrappers::Soundcloud
  require 'typhoeus'
  require 'json'

  def self.get_track_details(url)
    hydra = Typhoeus::Hydra.new
    request = Typhoeus::Request.new("http://api.soundcloud.com/resolve.json?url=#{url}&client_id=#{Constants::SoundcloudClientId}", :method => :get, :timeout => Constants::ApiTimeoutSeconds * 1000, :follow_location => true)
    hydra.queue(request)
    hydra.run
    response = request.response
    resp = ActiveSupport::JSON.decode(response.body)

    if !url.index("/sets/").nil?
      track = resp["tracks"][0]
      artist = resp["title"]
      title = track["title"]
      description = track["description"]
      thumbnail_url = track["artwork_url"]
      perma_link = track["permalink_url"]
    else
      title = resp['title']
      description = resp["description"]
      artist = resp["user"].present? ? resp["user"]["username"] : "Unknown"
      thumbnail_url = resp["artwork_url"]
      perma_link = resp["permalink_url"]
    end

    return {:artist => artist, :title => title, :description => description, :provider => "Soundcloud", :thumbnail_url => thumbnail_url, :perma_link => perma_link }
  end

  def self.get_thumbnail_url(url)
    hydra = Typhoeus::Hydra.new
    request = Typhoeus::Request.new("http://api.soundcloud.com/resolve.json?url=#{url}&client_id=#{Constants::SoundcloudClientId}", :method => :get, :timeout => Constants::ApiTimeoutSeconds * 1000, :follow_location => true)
    hydra.queue(request)
    hydra.run
    response = request.response
    resp = ActiveSupport::JSON.decode(response.body)

    if !url.index("/sets/").nil?
      return resp["tracks"][0]["artwork_url"]
    else
      return resp["artwork_url"]
    end
  end
end