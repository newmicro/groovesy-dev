class WallPost < ActiveRecord::Base
  belongs_to :user
  belongs_to :poster_user, :foreign_key => "poster_user_id", :class_name => "User"
  has_many :likes, :class_name => "WallPostLike", :foreign_key => "wall_post_id"
  has_many :comments, :class_name => "WallPostComment", :foreign_key => "wall_post_id"
  belongs_to :wall, :touch => true

  scope :repins, lambda { | slf | {:conditions => ["repin_parent_id=? OR repin_parent_id=?", slf.id, slf.repin_parent_id] } }
  scope :user_type, :conditions => ["post_type='user_wall'"]

  validates_presence_of :poster_user_id, :post_type

  attr_accessor :provider_id, :url

  def self.top_posts
    Rails.cache.fetch("top_posts") {
      ext_sites = []
      PostTextParser::PARSERS.each do | parser |
        ext_sites += eval( "UrlParsers::#{parser}::URL_MATCHES" )
      end

      where_clause = "((" + ext_sites.map{ | site | "post_text LIKE '%#{site}%'" }.join(" OR ") + ") AND repin_parent_id IS null)"

      WallPost.find_by_sql(
        "SELECT
            InnerQuery . *
         FROM
            (SELECT @lastUser:=0, @lastUserCount:=0) sqlvars,
            (SELECT
                MainQuery . *,
                    @lastUserCount:=if(@lastUser = MainQuery.poster_user_id, @lastUserCount + 1, 0) as TheCount,
                    @lastUser:=MainQuery.poster_user_id as carryover_user_id
            FROM
                (SELECT
                wall_posts . *,
                    (select
                            count(*)
                        from
                            wall_posts iwp
                        where
                            iwp.repin_parent_id = wall_posts.id) as rpins
            FROM
                wall_posts
            WHERE
                #{where_clause}
            ORDER BY wall_posts.poster_user_id ASC, created_at DESC, rpins DESC) MainQuery
            HAVING TheCount <= #{Constants::MaxPostsPerUserHomePage}) InnerQuery
        ORDER BY InnerQuery.created_at DESC , InnerQuery.rpins DESC
        LIMIT #{Constants::MaxPostsHomePage}"
      )
    }
  end

  def repin_content
    url = first_url
    if url.present?
      return url
    end
    return self.post_text
  end

  def total_repin_comments
    WallPostComment.count(:joins => ["INNER JOIN wall_posts ON wall_post_comments.wall_post_id = wall_posts.id"],
                         :conditions => ["wall_posts.id=? or wall_posts.repin_parent_id=?", self.id, self.id])
  end

  def repin_comments(offset = 0, limit = Constants::WallCommentsPerPage)
    WallPostComment.all(:offset => offset,
                         :limit => limit,
                         :select => "wall_post_comments.*",
                         :joins => ["INNER JOIN wall_posts ON wall_post_comments.wall_post_id = wall_posts.id"],
                         :conditions => ["wall_posts.id=? or wall_posts.repin_parent_id=?", self.id, self.id],
                         :order => "wall_post_comments.created_at ASC")
  end

  def delete_post
    self.likes.each do | l |
      nots = Notification.find_all_by_notification_type_and_notifiable_id("post_like", l.id)
      nots.each{ | n | n.destroy }
      l.destroy
    end

    self.comments.each do | c |
      nots = Notification.find_all_by_notification_type_and_notifiable_id("post_comment", c.id)
      nots.each{ | n | n.destroy }
      c.destroy
    end

    nots = Notification.find_all_by_notification_type_and_notifiable_id("user_wall_post", self.id)
    nots.each{ | n | n.destroy }

    # update any repin parent ids pointing to this record to null
    WallPost.update_all( "repin_parent_id=null", "repin_parent_id=#{self.id}" )

    self.destroy
  end

  def get_repins
    WallPost.count(:conditions => ["repin_parent_id = ? OR repin_parent_id = ?", self.repin_parent_id, self.id])
  end

  def is_youtube?
    urls = Util.extract_urls(self.post_text)
    if urls.size > 0
      if !PostTextParser.is_image?(urls.first)
        return UrlParsers::Youtube.match?(urls.first)
      end
    end
    return false
  end

  def first_url
    urls = Util.extract_urls(Util.strip_html(self.post_text))
    if urls.size > 0
      return urls.first
    end
    return nil
  end

  def player_url
    url = first_url
    if url.nil?
      return url
    end
    PostTextParser.player_url(url)
  end

  def commenters
    users = []
    self.comments.each{ | c | users << c.user }
    users
  end

  def create_notifications(comment)
    # notify post author
    Notification.create(:user_id => self.poster_user_id, :notifiable_type => "wall_post_comment", :notifiable_id => comment.id, :notification_type => "post_comment") if self.poster_user_id != comment.commenter_id

    # notify commenters
    commented = commenters.delete_if{ | c | c.id == comment.commenter_id || c.id == self.poster_user_id }
    if commented.size > 0
      commented.each do | user |
        Notification.create(:user_id => user.id, :notifiable_type => "wall_post_comment", :notifiable_id => comment.id, :notification_type => "comment_commented")
      end
    end
  end

  def preview_json
    { :provider_id => self.provider_id,
      :provider => self.api_provider,
      :artist => self.artist,
      :title => self.title,
      :url => self.url,
      :thumbnail_url => self.thumbnail_url}.to_json
  end

  def modal_link_text(logged_in)
    like_count = self.likes.size
    like_str = "#{like_count} #{like_count == 0 || like_count > 1 ? "People Like" : "Person Likes"} This"
    like_tpl = Constants::LikeCountTemplate + (logged_in ? Constants::LikeLinkTemplate : "")
    like_tpl.gsub("{like_count}", like_str).gsub("{post_id}", self.id.to_s)
  end

  def get_track_details
    urls = Util.extract_urls(self.post_text)

    if urls.size > 0

      self.url = urls.first
      parser = PostTextParser.find_parser(self.url)

      if parser.present?

        api = parser.downcase

        if api == "image"
          self.api_provider = "Image"
          self.meta_title = "A photo on Groovesy.com"
          self.is_video = false
          return
        end

        self.provider_id = UrlParsers::Youtube.get_id(self.url) if api == "youtube"

        track_dets = ApiWrappers.const_get(api.capitalize).send(:get_track_details, self.url)

        self.artist = track_dets[:artist]
        self.title = track_dets[:title]
        self.description = track_dets[:description]
        self.api_provider = track_dets[:provider]
        self.provider_perma_link = track_dets[:perma_link]
        self.meta_title = "#{self.artist} - #{self.title} via #{self.api_provider}"
        self.is_video = true
        self.thumbnail_url = track_dets[:thumbnail_url].present? ? track_dets[:thumbnail_url] : Constants::DefaultTrackThumbnail
        return
      end
    end
    self.api_provider = "Unknown"
    self.meta_title = "A post on Groovesy.com"
    self.is_video = false
  end
end