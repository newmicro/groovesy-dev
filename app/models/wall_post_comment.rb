class WallPostComment < ActiveRecord::Base
  belongs_to :user, :foreign_key => "commenter_id"
  belongs_to :wall_post, :touch => true

  validates_presence_of [:wall_post_id, :commenter_id, :comment]
end