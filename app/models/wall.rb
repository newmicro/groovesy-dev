class Wall < ActiveRecord::Base
  belongs_to :admin, :class_name => "User", :foreign_key => "admin_id"
  has_many :wall_members
  has_many :wall_users, :through => :wall_members
  has_many :posts, :class_name => "WallPost", :foreign_key => "wall_id", :conditions => "post_type='public_wall'"

  validates_presence_of [:name, :admin_id], :message => "is required"
  validates_presence_of :slug, :message => "is required", :if => Proc.new{|slf| !slf.name.blank? }
  validates_uniqueness_of :slug, :message => "is already taken"
  validates_length_of [:tags, :name], :maximum => 255

  def set_slug
    self.slug = Util.get_slug(self.name)
    x = 1
    while(!Wall.find_by_slug(self.slug).nil?) do # scary, but I think this will work
      self.slug = "#{Util.get_slug(self.name)}-#{x}"
      x += 1
    end
  end

  def self.random_walls
    Wall.all(:limit => Constants::MaxRandomWallsMyWallsPage, :order => "RAND()")
  end

  def self.search(tag)
    Wall.all(:conditions => ["tags LIKE ?", "%" + tag + "%"])
  end
end