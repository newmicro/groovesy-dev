class UserMailer < ActionMailer::Base
  def invite( user, email )
    @unsub_key = generate_unsubscribe(email)
    @user = user
    @dest_email = email
    mail(:from => "Groovesy.com <noreply@groovesy.com>", :to => email, :subject => "An invitation to #{Constants::Domain}")
  end

  def send_friend_request( to_user, from_user )
    @unsub_key = generate_unsubscribe(to_user.email)
    @to_user = to_user
    @from_user = from_user
    @dest_email = to_user.email
    mail(:from => "Groovesy.com <noreply@groovesy.com>", :to => to_user.email, :subject => "You've Received a New Friend Request")
  end

  def wall_post( from_user, to_user, post )
    @unsub_key = generate_unsubscribe(to_user.email)
    @to_user = to_user
    @from_user = from_user
    @dest_email = to_user.email
    @post = post
    mail(:from => "Groovesy.com <noreply@groovesy.com>", :to => to_user.email, :subject => "#{@from_user.first_and_last_name} posted on your wall")
  end

  def welcome_email(user)
    @unsub_key = generate_unsubscribe(user.email)
    @user = user
    @dest_email = user.email
    mail(:from => "Groovesy.com <noreply@groovesy.com>", :to => user.email, :subject => "Welcome to Groovesy")
  end

  def public_wall_post_notification(to_user, wall, post_user, post)
    @to_user = to_user
    @unsub_key = generate_unsubscribe(@to_user.email)
    @wall = wall
    @post_user = post_user
    @dest_email = @to_user.email
    @post = post
    mail(:from => "Groovesy.com <noreply@groovesy.com>", :to => @to_user.email, :subject => "#{@post_user.first_and_last_name} posted on #{@wall.name}")
  end

  def generate_unsubscribe(email)
    eu = EmailUnsubscribe.find_by_email(email)
    if eu.nil?
      key = UUID.new.generate
      EmailUnsubscribe.create(:email => email, :key => key, :set => false)
    else
      key = eu.key
    end
    return key
  end

  def retrieve_password( user )
    @user = user
    mail(:from => "Groovesy.com <noreply@groovesy.com>", :to => @user.email, :subject => "Reset password link")
  end
end