class PostTextParser

  PARSERS = ["Mixcloud", "Soundcloud", "Youtube"]

  def self.parse(wall_post, render_size, logged_in)
    txt = Util.strip_html(wall_post.post_text)
    urls = Util.extract_urls(txt)

    if urls.size > 0
      found_pinnable_url = false
      urls.each do | url |
        if Util.valid_url?(url)
          parser = PostTextParser.find_parser(url)
          if parser.nil?
            txt.gsub!(url, "<a href='#{url}' target='_blank'>#{url.size > 35 ? url[0..35] + '...' : url}</a>")
          elsif found_pinnable_url # 2nd parseable link?  kill it
            txt.gsub!(url, "")
          else
            found_pinnable_url = true
            UrlParsers.const_get(parser).send(:get_tpl, txt, url, wall_post, render_size, logged_in)
          end
        end
      end
    end

    txt
  end

  def self.player_url(url)
    if PostTextParser.is_image?(url)
      return url
    end

    parser = PostTextParser.find_parser( url )
    UrlParsers.const_get(parser).send(:player_url, url)
  end

  def self.is_image?(url)
    [".gif", ".jpg", ".png", ".jpeg"].include?(Pathname.new(Addressable::URI.heuristic_parse(url).path).extname)
  end
  
  def self.find_parser(url)
    if PostTextParser.is_image?(url)
      return "Image"
    end

    p = nil
    PARSERS.each do | parser |
      p = parser if UrlParsers.const_get(parser).send(:match?, url)
    end
    p
  end
end