class UrlParsers::Youtube
  TEMPLATE_SMALL = "<div style='position: relative'><div class='vid_title'>{title}</div><a href='http://www.youtube.com/embed/{id}?autoplay=1&fs=1' class='colorboxvid' id='post_vid_link_{post_id}' data-provider='Youtube' data-id='{post_id}' like-html='{like_html}'><img width='229' height='200' src='{thumbnail_url}' /></a></div>"
  TEMPLATE_MEDIUM = "<div style='position: relative'><div class='vid_title_medium'>{title}</div><a href='http://www.youtube.com/embed/{id}?autoplay=1&fs=1' class='colorboxvid' id='post_vid_link_{post_id}' data-provider='Youtube' data-id='{post_id}' like-html='{like_html}'><img width='430' height='430' src='{thumbnail_url}' /></a></div>"

  PLAYER_TEMPLATE = "https://www.youtube.com/v/{id}"

  URL_MATCHES = ["youtube.com", "youtu.be"]

  def self.match?(url)
    URL_MATCHES.each do | u |
      if !url.index(u).nil?
        return true
      end
    end
    false
  end

  def self.player_url(url)
    PLAYER_TEMPLATE.gsub("{id}", UrlParsers::Youtube.get_id(url))
  end

  def self.get_id(url)
    if url.nil?
      return ""
    end

    uri = Addressable::URI.heuristic_parse(url)

    id = ""
    if uri.query.blank?
      if !uri.path.blank?
        if uri.path.split("/").size > 0
          id = uri.path.split("/")[1] #youtu.be type links
        end
      end
    else
      parsed = CGI::parse(uri.query)
      id = parsed["v"].first if parsed.has_key? "v"
    end
    return id
  end

  def self.get_tpl(text, url, wall_post, render_size, logged_in)
    id = UrlParsers::Youtube.get_id(url)
    if !id.blank?
      text.gsub!(url, (self.const_get("TEMPLATE_#{render_size.upcase}").gsub("{like_html}", wall_post.modal_link_text(logged_in)).
                                                                       gsub("{id}", id)).
                                                                       gsub("{post_id}", wall_post.id.to_s).
                                                                       gsub("{thumbnail_url}", wall_post.thumbnail_url).
                                                                       gsub("{title}", "#{wall_post.title}<br />by #{wall_post.artist}"))
    else
      text.gsub!(url, "<a href='#{url}' target='_blank'>#{url}</a>")
    end

    text
  end
end