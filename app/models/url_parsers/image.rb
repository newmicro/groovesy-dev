class UrlParsers::Image
  TEMPLATE_SMALL = "<a href='{param}' class='colorboximg' data-provider='Image' like-html='{like_html}'><img src='{param}' border='0' width='229'/></a>"
  TEMPLATE_MEDIUM = "<a href='{param}' class='colorboximg' data-provider='Image' like-html='{like_html}'><img src='{param}' border='0' width='430'/></a>"

  def self.get_tpl(text, url, wall_post, render_size, logged_in)
    text.gsub!(url, self.const_get("TEMPLATE_#{render_size.upcase}").gsub("{like_html}", wall_post.modal_link_text(logged_in)).
                                                                    gsub("{param}", url).
                                                                    gsub("{post_id}", wall_post.id.to_s))

    text
  end
end