class UrlParsers::Soundcloud
  #TEMPLATE_SMALL = "<object height='229' width='229'> <param name='movie' value='http://player.soundcloud.com/player.swf?url={param}&amp;auto_play=false&amp;player_type=artwork&amp;color=0066cc'></param><param name='wmode' value='transparent' /><param name='allowscriptaccess' value='always'></param><embed allowscriptaccess='always' height='229' src='http://player.soundcloud.com/player.swf?url={param}&amp;auto_play=false&amp;player_type=artwork&amp;color=0066cc' type='application/x-shockwave-flash' width='229' wmode='transparent'></embed></object>"
  #TEMPLATE_MEDIUM = "<object height='430' width='430'> <param name='movie' value='http://player.soundcloud.com/player.swf?url={param}&amp;auto_play=false&amp;player_type=artwork&amp;color=0066cc'></param><param name='wmode' value='transparent' /><param name='allowscriptaccess' value='always'></param><embed allowscriptaccess='always' height='430' src='http://player.soundcloud.com/player.swf?url={param}&amp;auto_play=false&amp;player_type=artwork&amp;color=0066cc' type='application/x-shockwave-flash' width='430' wmode='transparent'></embed></object>"
  #TEMPLATE_SMALL = "<iframe width='229' height='166' scrolling='no' frameborder='no' src='https://w.soundcloud.com/player/?url={param}'></iframe>"
  TEMPLATE_SMALL = "<div style='position: relative;'><div class='vid_title'>{title}</div><a href='https://w.soundcloud.com/player/?url={param}' data-provider='Soundcloud' data-id='{post_id}' like-html='{like_html}' class='colorboxvid'><img width='229' height='200' src='{thumbnail_url}' /></a></div>"
  TEMPLATE_MEDIUM = "<iframe width='520' height='166' scrolling='no' frameborder='no' src='https://w.soundcloud.com/player/?url={param}'></iframe>"
  PLAYER_TEMPLATE = "http://player.soundcloud.com/player.swf?url={param}&amp;auto_play=false&amp;player_type=artwork&amp;color=0066cc"

  URL_MATCHES = ["soundcloud.com"]

  def self.match?(url)
    URL_MATCHES.each do | u |
      if !url.index(u).nil?
        return true
      end
    end
    false
  end

  def self.player_url(url)
    PLAYER_TEMPLATE.gsub("{param}", url)
  end

  def self.get_tpl(text, url, wall_post, render_size, logged_in)
    text.gsub!(url, self.const_get("TEMPLATE_#{render_size.upcase}").gsub("{param}", url).
                                                                     gsub("{post_id}", wall_post.id.to_s).
                                                                     gsub("{thumbnail_url}", wall_post.thumbnail_url).
                                                                     gsub("{title}", "#{wall_post.title}<br />by #{wall_post.artist}").
                                                                     gsub("{like_html}", wall_post.modal_link_text(logged_in)))
    text
  end
end