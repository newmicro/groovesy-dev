class UrlParsers::Mixcloud
  TEMPLATE_SMALL = "<object width='229' height='220'><param name='movie' value='http://www.mixcloud.com/media/swf/player/mixcloudLoader.swf?feed={param}&stylecolor=&embed_type=widget_standard'></param><param name='allowFullScreen' value='true'></param><param name='wmode' value='opaque'></param><param name='allowscriptaccess' value='always'></param><embed src='http://www.mixcloud.com/media/swf/player/mixcloudLoader.swf?feed={param}&stylecolor=&embed_type=widget_standard' type='application/x-shockwave-flash' wmode='opaque' allowscriptaccess='always' allowfullscreen='true' width='229' height='220'></embed></object>"
  TEMPLATE_MEDIUM = "<object width='430' height='430'><param name='movie' value='http://www.mixcloud.com/media/swf/player/mixcloudLoader.swf?feed={param}&stylecolor=&embed_type=widget_standard'></param><param name='allowFullScreen' value='true'></param><param name='wmode' value='opaque'></param><param name='allowscriptaccess' value='always'></param><embed src='http://www.mixcloud.com/media/swf/player/mixcloudLoader.swf?feed={param}&stylecolor=&embed_type=widget_standard' type='application/x-shockwave-flash' wmode='opaque' allowscriptaccess='always' allowfullscreen='true' width='430' height='430'></embed></object>"
  PLAYER_TEMPLATE = "http://www.mixcloud.com/media/swf/player/mixcloudLoader.swf?feed={param}&stylecolor=&embed_type=widget_standard"

  URL_MATCHES = ["mixcloud.com"]

  def self.match?(url)
    URL_MATCHES.each do | u |
      if !url.index(u).nil?
        return true
      end
    end
    false
  end

  def self.player_url(url)
    PLAYER_TEMPLATE.gsub("{param}", url)
  end

  def self.get_tpl(text, url, wall_post, render_size, logged_in)
    text.gsub!(url, self.const_get("TEMPLATE_#{render_size.upcase}").gsub("{param}", url))
    text
  end
end