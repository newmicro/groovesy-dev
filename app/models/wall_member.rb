class WallMember < ActiveRecord::Base
  belongs_to :wall_user, :class_name => "User", :foreign_key => "user_id"
  belongs_to :member_wall, :class_name => "Wall", :foreign_key => "wall_id", :touch => true

  validates_presence_of [:wall_id, :user_id], :message => "is required"
end