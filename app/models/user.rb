class User < ActiveRecord::Base
  require 'digest/sha1'

  has_many :wall_pinnings
  has_many :pinned, :class_name => "WallPinning", :foreign_key => "pinned_user_id"
  has_many :wall_posts
  has_many :posted_posts, :class_name => "WallPost", :foreign_key => "poster_user_id"
  has_many :friendships, :class_name => "FriendUser"
  has_many :friends, :through => :friendships
  has_many :inverse_friendships, :class_name => "FriendUser", :foreign_key => "friend_user_id"
  has_many :inverse_friends, :through => :inverse_friendships, :source => :user
  has_one :photo
  has_many :admin_walls, :class_name => "Wall", :foreign_key => "admin_id"
  has_many :wall_members
  has_many :member_walls, :through => :wall_members
  has_many :notifications, :order => "acknowledged ASC, created_at DESC"
  has_many :unacknowledged_notifications, :class_name => "Notification", :foreign_key => "user_id", :conditions => "acknowledged=0"
  has_many :walls, :foreign_key => "admin_id"

  validates_presence_of :first_name, :last_name, :email
  validates_presence_of :password, :if => Proc.new{ | this | !this.cancel_password_validation }
  validates_uniqueness_of :email
  validates_confirmation_of :password

  validates_length_of :first_name, :maximum => 25, :if => Proc.new{ | this | !this.first_name.blank? }
  validates_length_of :last_name, :maximum => 255, :if => Proc.new{ | this | !this.last_name.blank? }
  validates_length_of :email, :maximum => 65

  attr_accessor :password_confirmation
  attr_accessor :cancel_password_validation

  def search_friends( term )
    inv_fr = self.friends.all(:conditions => ["(first_name like ? OR last_name like ? OR CONCAT(first_name, ' ', last_name) like ? OR email like ?) AND friend_users.status='confirmed'", term + "%", term + "%", term + "%", term + "%"])
    fr_inv = self.inverse_friends.all(:conditions => ["(first_name like ? OR last_name like ? OR CONCAT(first_name, ' ', last_name) like ? OR email like ?) AND friend_users.status='confirmed'", term + "%", term + "%", term + "%", term + "%"])
    inv_fr + fr_inv
  end

  def self.search_friend( obj, exclude_user )
    if obj[:first_name].present? || obj[:last_name].present? || obj[:email].present?
      exclude_users = [exclude_user] + exclude_user.friends + exclude_user.inverse_friends
      conditions = []
      conditions << ["first_name=?", obj[:first_name]] if !obj[:first_name].blank?
      conditions << ["last_name=?", obj[:last_name]] if !obj[:last_name].blank?
      conditions << ["email=?", obj[:email]] if !obj[:email].blank?

      bind = conditions.map{ | c | c[ 1 ] }
      bind << exclude_users.flatten
      conditions << ["id NOT IN (?)", "unused"]

      User.all(:conditions => [conditions.map{|c|c.first}.join(" AND "), *bind ])
    else
      []
    end
  end

  def self.new_members
    User.includes(:photo).order("created_at DESC").limit(Constants::MaxMembersHomePage)
  end

  def recent_wall_posts(offset = 0, limit = nil)
    WallPost.all(:conditions => ["user_id = ? || poster_user_id = ?", self.id, self.id],
                 :offset => offset,
                 :select => "wall_posts.*",
                 :limit => limit,
                 :order => "wall_posts.created_at DESC",
                 :include => [:likes, {:comments => {:user => :photo}}])
  end

  def request_friendship(friend_id)
    fr_req = self.inverse_friendships.first(:conditions => ["user_id=? AND friend_users.status='requested'", friend_id])
    if !fr_req.nil? # if they've already requested of me, confirm
      fr_req.update_attribute(:status, "confirmed")
      Notification.create(:user_id => friend_id, :notifiable_type => "friend_user", :notifiable_id => fr_req.id, :notification_type => "friend_request_confirmed")
    else
      if self.friends.first(:conditions => ["friend_user_id=?", friend_id]).nil?
        fr_req = FriendUser.create( { :user_id => self.id, :friend_user_id => friend_id, :status => "requested" } )
        Notification.create(:user_id => friend_id, :notifiable_type => "friend_user", :notifiable_id => fr_req.id, :notification_type => "friend_requested")
      end
    end
  end

  def password
    @password
  end

  def password=( pwd )
    @password = pwd
    return if pwd.blank?
    create_new_salt
    self.password_hash = User.encrypted_password( self.password, self.salt )
  end

  def self.authenticate( email, password )
    user = self.find_by_email( email )
    if user
      expected_password = encrypted_password( password, user.salt )
      user = nil if user.password_hash != expected_password
    end
    user
  end

  def self.generate_new_guid
    UUID.new.generate
  end

  def create_default_wallpinnings
    WallPinning.create({:user_id => self.id, :slot => 0, :pinned_user_id => self.id})
  end

  def can_post?(pinning)
    pu = pinning.pinned_user
    !pu.nil? && ( is_me?(pu.id) || is_my_friend?(pu.id) )
  end

  def is_me?(id)
    self.id == id.to_i
  end

  def is_my_friend?(id)
    conf_fr = self.friends.all(:conditions => ["friend_users.status='confirmed' AND users.id=?", id.to_i])
    conf_inv_fr = self.inverse_friends.all(:conditions => ["friend_users.status='confirmed' AND users.id=?", id.to_i])

    return true if conf_fr.size > 0 || conf_inv_fr.size > 0
    return false
  end

  def friend_request_pending?(id)
    conf_fr = self.friends.all(:conditions => ["friend_users.status='requested' AND users.id=?", id])
    conf_inv_fr = self.inverse_friends.all(:conditions => ["friend_users.status='requested' AND users.id=?", id])

    return true if conf_fr.size > 0 || conf_inv_fr.size > 0
    return false
  end

  def become_friend(friend)
    if can_request_friendship?(friend)
      FriendUser.create(:user_id => friend.id, :friend_user_id => self.id, :status => "confirmed", :acknowledged => false)
    end
  end

  def can_request_friendship?(profile)
    !self.is_my_friend?(profile.id) && self != profile && !friend_request_pending?(profile.id)
  end

  def pinning_in_slot(pinnings, slot)
    ps = pinnings.select{ | p | p.slot == slot }
    ps.size > 0 ? ps.first : nil
  end

  def confirmed_friends
    conf_fr = self.friends.all(:conditions => ["friend_users.status='confirmed'"])
    conf_inv_fr = self.inverse_friends.all(:conditions => ["friend_users.status='confirmed'"])
    conf_fr + conf_inv_fr
  end

  def new_friends
    self.friends.all(:conditions => ["friend_users.status='confirmed' AND friend_users.acknowledged=0"])
  end

  def acknowledge_new_friends
    FriendUser.update_all("acknowledged=1", "user_id=#{self.id} AND status='confirmed' AND acknowledged=0")
  end

  def friend_requests
    self.inverse_friendships.all(:conditions => ["friend_users.status='requested'"])
  end

  def requested_friends
    self.friendships.all(:conditions => ["friend_users.status='requested'"])
  end

  def remove_friendship(friend_id)
    # update his wall pinnings
    WallPinning.update_all( "pinned_user_id=null", "pinned_user_id=#{self.id} AND user_id=#{friend_id}" )

    # update my wall pinnings
    WallPinning.update_all( "pinned_user_id=null", "pinned_user_id=#{friend_id} AND user_id=#{self.id}" )

    # delete friend user relationships
    fru = FriendUser.all(:conditions => "(user_id=#{self.id} AND friend_user_id=#{friend_id}) OR (user_id=#{friend_id} AND friend_user_id=#{self.id})")
    fru.each do | fu |
      nots = Notification.find_all_by_notification_type_and_notifiable_id("friend_request_confirmed", fu.id)
      nots.each{ | n | n.destroy }
      nots = Notification.find_all_by_notification_type_and_notifiable_id("friend_requested", fu.id)
      nots.each{ | n | n.destroy }
      fu.destroy
    end
  end

  def first_and_last_name
    self.first_name + " " + self.last_name
  end

  def self.index_worthy_activity
    User.all(:select => "user.*, wall_posts.*, count(wall_posts.id) as 'total_posts'", :include => :wall_posts, :conditions => ["wall_posts.id > 0"], :group => "wall_posts.id", :limit => Constants::MaxWallsHomePage)
  end

  def destroy_profile
    # delete friendships
    conf_friends = self.confirmed_friends
    conf_friends.each do | friend |
      self.remove_friendship(friend.id)
    end

    # delete friend requests
    reqs = self.friend_requests + self.requested_friends
    reqs.each do | req |
      nots = Notification.find_all_by_notification_type_and_notifiable_id("friend_requested", req.id)
      nots.each{ | n | n.destroy }
      req.destroy
    end

    # delete wall_pinnings
    self.wall_pinnings.each{ | pinning | pinning.destroy }

    # delete photo
    if !self.photo.nil?
      ph = self.photo
      photo_path = "#{RAILS_ROOT}/public/images/uploads/"
      File.delete( "#{photo_path}#{ph.id}.#{ph.file_ext}" ) if File.exists? "#{photo_path}#{ph.id}.#{ph.file_ext}"
      File.delete( "#{photo_path}#{ph.id}_128x128.#{ph.file_ext}" ) if File.exists? "#{photo_path}#{ph.id}_128x128.#{ph.file_ext}"
      File.delete( "#{photo_path}#{ph.id}_64x64.#{ph.file_ext}" ) if File.exists? "#{photo_path}#{ph.id}_64x64.#{ph.file_ext}"
      File.delete( "#{photo_path}#{ph.id}_32x32.#{ph.file_ext}" ) if File.exists? "#{photo_path}#{ph.id}_32x32.#{ph.file_ext}"
      File.delete( "#{photo_path}#{ph.id}_15x15.#{ph.file_ext}" ) if File.exists? "#{photo_path}#{ph.id}_15x15.#{ph.file_ext}"
      ph.destroy
    end

    # delete posts
    posts = self.wall_posts + self.posted_posts
    posts.each do | p |
      p.delete_post
    end

    # delete comments
    comments = WallPostComment.all(:conditions => ["commenter_id=?", self.id])
    comments.each do | c |
      nots = Notification.find_all_by_notification_type_and_notifiable_id("post_comment", c.id)
      nots.each{ | n | n.destroy }
      c.destroy
    end

    # re-assign walls
    admin = User.find_by_email("blakeleemiller@gmail.com")
    self.admin_walls.each do | wall |
      wall.update_attribute(:admin_id, admin.id)
      admin.join_wall(wall)
    end

    self.destroy
  end

  def member_of?(wall)
    self.member_walls.each do | w |
      return true if w == wall
    end
    return false
  end

  def join_wall(wall)
    if !member_of?(wall)
      self.wall_members << WallMember.create(:wall_id => wall.id, :user_id => self.id)
      self.save
    end
  end

  def leave_wall(wall)
    if member_of?(wall)
      self.wall_members.find_by_wall_id(wall.id).destroy
    end
  end

  def friend_posts
    WallPost.includes([:likes, {:comments => {:user => :photo}}]).where("poster_user_id IN (?)", confirmed_friends).order("wall_posts.created_at DESC").limit(Constants::MaxPostsWall)
  end

  def likes
    WallPost.joins("INNER JOIN wall_post_likes ON wall_post_likes.wall_post_id = wall_posts.id").where("liker_id=?", self.id).includes({:comments => {:user => :photo}}).order("wall_posts.created_at DESC").limit(Constants::MaxPostsWall)
  end

  def posts
    WallPost.where("wall_posts.user_id=?", self.id).order("wall_posts.created_at DESC").limit(Constants::MaxPostsWall)
  end

  def location
    "#{city}, #{state}"
  end

  private

  def self.encrypted_password( password, salt )
    string_to_hash = password + "wablledeedoo" + salt # 'wibble' makes it harder to guess
    Digest::SHA1.hexdigest( string_to_hash )
  end

  def create_new_salt
    self.salt = self.object_id.to_s + rand.to_s
  end
end