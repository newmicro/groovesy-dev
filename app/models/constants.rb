module Constants
  WallPostsPerPage = 5
  WallCommentsPerPage = 5
  SinglePostCommentsPerPage = 12
  SiteName = "Groovesy"
  Domain = "Groovesy.com"
  MaxWallsHomePage = 15
  MaxPostsHomePage = 22
  MaxPostsPublicWall = 15
  MaxPostsWall = 28
  MaxRandomWallsMyWallsPage = 5
  MaxPostsPerUserHomePage = 5
  ShowPosts = true
  DeliverMail = true
  ProfileWallListLimit = 10
  MaxNotificationsInPopup = 5
  MaxNotificationsIndex = 30
  ApiTimeoutSeconds = 15
  MaxMembersHomePage = 5
  SoundcloudClientId = "6f040e3a3854c25324988dfc3cb5f7b6"
  SoundcloudClientSecret = "6ec1545973e207e3193a3547d100ab18"
  DefaultTrackThumbnail = "http://www.groovesy.com/images/default_thumbnail.jpg"
  Providers = ["Soundcloud", "Mixcloud", "Youtube"]
  LikeCountTemplate = "<span class=\"like_count_modal\" data-id=\"{post_id}\">{like_count}</span>"
  LikeLinkTemplate = "&nbsp;&nbsp;<a class=\"modal\" data-remote=\"true\" href=\"/streams/like/{post_id}\">Like</a>"
end
