class WallMemberSweeper < ActionController::Caching::Sweeper
  observe WallMember

  def after_save(wm)
    expire_cache(wm)
  end

  def after_destroy(wm)
    expire_cache(wm)
  end

  def expire_cache(wm)
  end
end