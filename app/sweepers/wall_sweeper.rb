class WallSweeper < ActionController::Caching::Sweeper
  observe Wall

  def after_save(wall)
    expire_cache(wall)
  end

  def after_destroy(wall)
    expire_cache(wall)
  end

  def expire_cache(wall)
  end
end