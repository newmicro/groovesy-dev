class WallPostSweeper < ActionController::Caching::Sweeper
  observe WallPost

  def after_save(wall_post)
    expire_cache(wall_post)
  end

  def after_destroy(wall_post)
    expire_cache(wall_post)
  end

  def expire_cache(wall_post)
    expire_page :controller => 'content', :action => 'index'
    Rails.cache.delete "top_posts" if Rails.cache.exist?("top_posts")
  end
end