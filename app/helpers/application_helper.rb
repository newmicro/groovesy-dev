module ApplicationHelper
  def render_flash( flash )
    return render_msg( :notice, flash[ :notice ] ) if !flash[ :notice ].blank?
    return render_msg( :error, flash[ :error ] ) if !flash[ :error ].blank?
  end

  def render_scoped_flash( flash, scope )
    return render_msg( :notice, flash[ :notice ].message ) if !flash[ :notice ].blank? && flash[ :notice ].is_a?( ScopedFlash ) && flash[ :notice ].scope == scope
    return render_msg( :error, flash[ :error ].message ) if !flash[ :error ].blank? && flash[ :error ].is_a?( ScopedFlash ) && flash[ :error ].scope == scope
  end

  def render_msg( scope, msg )
    klass = scope == :notice ? 'success' : 'alert'
    "<div class='alert-box #{klass}'>#{ msg }<a href='' class='close'>&times;</a></div>"
  end

  def render_profile_photo( user, dimensions, klass=nil )
    if !user.nil? && !user.photo.nil?
      image_tag( "/images/uploads/#{user.photo.id}_#{dimensions}.#{user.photo.file_ext}", :class => "profile-photo #{klass}", :align => "left", :alt => user.first_and_last_name, :title => user.first_and_last_name )
    else
      image_tag( "default_profile_#{dimensions}.jpg", :class => "profile-photo", :width => dimensions.split("x")[0], :class => klass, :height => dimensions.split("x")[1], :align => "left", :alt => user.first_and_last_name, :title => user.first_and_last_name )
    end
  end

  def render_notification_count(user)
    count = user.unacknowledged_notifications.size

    return "<span id='notif_count'>+#{ count }</span>" if count > 0
    return ""
  end

  def custom_time_ago_in_words(time_str)
    time = time_str.to_time
    "#{time_ago_in_words(time)} ago"
  end

  def render_friend_count(profile)
    count = profile.confirmed_friends.count
    "#{pluralize(count, 'friend')}"
  end

  def process_post_text( wall_post, render_size, logged_in )
    PostTextParser.parse(wall_post, render_size, logged_in)
  end

  def escape_single_quotes(str)
    str.gsub(/[']/, '\\\\\'')
  end

  def format_fb_share_desc(desc)
    if desc.blank?
      return ""
    end
    strip_returns(desc.gsub(/[']/, '\\\\\''))
  end

  def strip_returns(val)
    val.gsub(/[\n\r]/, " ")
  end

  def dotted_linkify(str)
    urls = Util.extract_urls(str)
    if urls.size > 0
      urls.each do | url |
        str.gsub!(url, "<a href='#{url}' target='_blank'>#{url.size > 35 ? url[0..35] + '...' : url}</a>")
      end
    end
    str
  end
end