module WallHelper
  def render_new_fr_count(user)
    count = user.new_friends.size + user.friend_requests.size
    
    return "<span id='fr_count'>+#{ count }</span>" if count > 0
    return ""
  end

  def render_tags(tags)
    result = ""
    if tags.blank?
      return "No tags yet"
    end
    tags.split(",").each_with_index do | tag, index |
      tag.strip!
      result += link_to(tag, wall_search_url(tag)) + ( index < tags.split(",").size - 1  ? "|" : "" )
    end
    result
  end
end