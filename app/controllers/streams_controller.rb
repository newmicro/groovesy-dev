class StreamsController < ApplicationController
  layout "full_page"
  before_filter :init, :except => [:ajax_load_posts, :ajax_load_likes, :ajax_load_comments]

  def init
    @active_tab = "streams"
    if @user.nil?
      redirect_to login_index_url
    end
  end

  def index
    get_wall_configs
    @wall_post = WallPost.new
    @wall_pinning = WallPinning.new
    render :layout => "full_page"
  end

  def ajax_post
    @wall_pinning_id = params["wall_pinning_id"]
    user_is_me = @user.is_me?(params[:wall_post][:user_id])

    if !params[:wall_post][:post_text].blank?
      @wall_post = WallPost.new(params[:wall_post])
      @wall_post.post_type = "user_wall"
      @wall_post.poster_user = @user
      Util.strip_html(@wall_post.post_text)
      @wall_post.get_track_details
      @wall_post.save
      Notification.create(:user_id => @wall_post.user_id, :notifiable_type => "wall_post", :notifiable_id => @wall_post.id, :notification_type => "user_wall_post") if @user.id != @wall_post.user_id
      UserMailer.deliver_wall_post( @user, @wall_post.user, @wall_post ) if !user_is_me && Constants::DeliverMail && EmailUnsubscribe.find_by_email_and_set(@wall_post.user.email, true).nil?
      respond_to do |format|
        format.js{ render "streams/ajax/ajax_post" }
      end
    else
      render :text => ""
    end
  end

  def pin_user
    @wall_pinning = WallPinning.new(params[:wall_pinning])

    if( @user.is_my_friend?(@wall_pinning.pinned_user_id) )
      @wall_pinning.slot = WallPinning.next_slot(@user.id)
      @wall_pinning.user_id = @user.id
      @wall_pinning.save
      flash.now[:notice] = "Your friend was pinned"
    end

    @wall_post = WallPost.new
    @wall_pinning = WallPinning.new

    get_wall_configs

    render :template => "streams/index"
  end

  def ajax_unpin_user
    @wall_pinning = WallPinning.find(params[:id])
    @old_wp_id = @wall_pinning.id

    if @wall_pinning.user_id == @user.id
      WallPinning.reorg_slots(@wall_pinning)
      @wall_pinning.destroy
    end

    respond_to do |format|
      format.js{ render "streams/ajax/ajax_unpin_user" }
    end
  end

  def ajax_load_posts
    @wall_pinning = WallPinning.find(params["wall_pinning_id"])
    @posts = @wall_pinning.pinned_user.recent_wall_posts(params["page"].to_i * Constants::WallPostsPerPage, Constants::WallPostsPerPage)
    respond_to do |format|
      format.js{ render "streams/ajax/ajax_load_posts" }
    end
  end

  def ajax_like
    @post = WallPost.find(params[:id])

    if WallPostLike.first(:conditions => ["wall_post_id=? AND liker_id=?", params[:id].to_i, @user.id]).nil?
      wpl = WallPostLike.create({:wall_post_id => params[:id], :liker_id => @user.id})
      Notification.create(:user_id => @post.poster_user_id, :notifiable_type => "wall_post_like", :notifiable_id => wpl.id, :notification_type => "post_like") if @user.id != @post.poster_user_id
      @post = WallPost.find(params[:id])
      respond_to do |format|
        format.js{ render "streams/ajax/ajax_like" }
      end
    else
      render :text => ""
    end
  end

  def ajax_repin
    @post = WallPost.find(params[:id])
    @new_post = WallPost.new
    @new_post.attributes = @post.attributes
    @new_post.user_id = @user.id
    @new_post.poster_user_id = @user.id
    @new_post.repin_parent_id = @post.repin_parent_id.nil? ? @post.id : @post.repin_parent_id
    #@repin_text = @post.repin_content
    respond_to do |format|
      format.js{ render "streams/ajax/ajax_repin" }
    end
  end

  def ajax_post_repin
    @post = WallPost.new(params[:wall_post])
    if !@user.nil? && @user.is_me?(@post.user_id)
      Util.strip_html(@post.post_text)
      @post.post_text += "<br />#{params[:repin_content]}"
      @post.post_type = "user_wall"
      @post.get_track_details
      @post.save
      respond_to do |format|
        format.js{ render "streams/ajax/ajax_post_repin" }
      end
    else
      render :text => ""
    end
  end

  def ajax_comment
    @post = WallPost.find(params[:id])
    @comment = WallPostComment.new({:wall_post_id => @post.id, :commenter_id => @user.id, :comment => params[:wall_post_comment][:comment]})
    Util.strip_html(@comment.comment)
    if @comment.valid?
      @comment.save
      @post.create_notifications(@comment)
      respond_to do |format|
        format.js{ render "streams/ajax/ajax_comment" }
      end
      return
    end
    render :text => ""
  end

  def ajax_load_likes
    @post = WallPost.find(params[:id])
    respond_to do |format|
      format.js{ render "streams/ajax/ajax_load_likes" }
    end
  end

  def ajax_load_comments
    @post = WallPost.find(params[:post_id])
    @comments = @post.comments.all(:include => {:user => :photo},
                                    :limit => Constants::WallCommentsPerPage,
                                    :offset => (params["page"].to_i * Constants::WallCommentsPerPage),
                                    :order => "created_at ASC")
    if @comments.size > 0
      respond_to do |format|
        format.js{ render "streams/ajax/ajax_load_comments" }
      end
      return
    end
    render :text => ""
  end

  def ajax_delete_post
    @post = WallPost.find(params[:id])
    if !@post.nil?
      @post_id = @post.id
      if [@post.user, @post.poster_user].include? @user
        @post.delete_post
        respond_to do |format|
          format.js{ render "streams/ajax/ajax_delete_post" }
        end
        return
      end
    end
    render :text => ""
  end


  private

  def get_wall_configs
    wall_pinnings = @user.wall_pinnings.all(:include => {:pinned_user => [:wall_posts, :photo]}, :order => "slot ASC")

    @wall_configs = []
    wall_pinnings.each do | wp |
      @wall_configs << WallConfig.new({:pinning => wp, :on_your_page => true })
    end
  end
end