class ContentController < ApplicationController
  layout "full_page"

  caches_page :index, :privacy_policy, :facebook_app
  cache_sweeper :wall_post_sweeper, :only => [:index]

  def index
    @posts = WallPost.top_posts
    #@new_members = User.new_members
    render :layout => "full_page_isotope" #full_page_isotope_right_sidebar"
  end

  def privacy_policy
  end

  #def boo
  #  reset_session
  #  cookies.delete( :remember_me_id ) if cookies[ :remember_me_id ]
  #  cookies.delete( :remember_me_code ) if cookies[ :remember_me_code ]
  #  render :text => ""
  #end

  def ajax_load_comments
    @post = WallPost.find(params[:post_id])
    @comments = @post.repin_comments(params["page"].to_i * Constants::WallCommentsPerPage)
  end

  def ajax_comment
    @post = WallPost.find(params[:id])

    if !@user.nil?
      @comment = WallPostComment.new({:wall_post_id => params[:id], :commenter_id => @user.id, :comment => params[:wall_post_comment][:comment]})
      Util.strip_html(@comment.comment)
      @comment.save
      @post.create_notifications(@comment)
    else
      render :text => ""
    end
  end

  def facebook_app
    render :template => "content/facebook_app", :layout => "facebook"
  end

  def facebook_invited
    flash[:notice] = "Your friends have been invited!"
    redirect_to friends_url
  end
end