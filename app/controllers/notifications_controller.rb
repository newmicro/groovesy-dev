class NotificationsController < ApplicationController

  def index
    @active_tab = "notifications"
    @notifications = @user.notifications.all(:limit => Constants::MaxNotificationsIndex)
    @unack_list = @notifications.select{| n | !n.acknowledged }.collect{ | n | n.id }.join(",")
  end

  def ajax_load
    @notifications = @user.notifications.all(:limit => Constants::MaxNotificationsInPopup)
    @unack_list = @notifications.select{| n | !n.acknowledged }.collect{ | n | n.id }.join(",")
  end

  def ajax_ack
    if !params["ids"].blank?
      @user.notifications.update_all("acknowledged=1", ["id in (?)", params["ids"].split(",")])
      @new_unack_count = @user.unacknowledged_notifications.size
    else
      render :text => ""
    end
  end
end