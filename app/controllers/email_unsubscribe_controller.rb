class EmailUnsubscribeController < ApplicationController
  layout "application"

  def index
    eu = EmailUnsubscribe.find_by_key(params[:key])
    if eu.nil?
      flash.now[:error] = "You are not in our system, or the link you clicked was invalid"
    else
      eu.update_attribute(:set, true)
      flash.now[:notice] = "You have been removed from future mailings"
    end
  end
end