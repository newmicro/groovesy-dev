class PostsController < ApplicationController
  layout "posts"

  def show
    @wall_post = WallPost.find_by_id(params[:id])
    if !@wall_post.nil? && @wall_post.api_provider != "Unknown"
      @player_url = @wall_post.player_url
    end
  end
end