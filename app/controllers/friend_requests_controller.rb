class FriendRequestsController < ApplicationController
  def new
    @user.request_friendship(params[:id])
    
    # email the recipient
    u = User.find(params[:id])
    UserMailer.deliver_send_friend_request( u, @user ) if Constants::DeliverMail && EmailUnsubscribe.find_by_email_and_set(u.email, true).nil?

    flash.now[:notice] = "Your request was sent"
    render :template => "friends/find"
  end

  def ajax_new
    @user.request_friendship( params[ :id ] )

    # email the recipient
    u = User.find(params[:id])
    UserMailer.deliver_send_friend_request( u, @user ) if Constants::DeliverMail && EmailUnsubscribe.find_by_email_and_set(u.email, true).nil?
  end

  def update
    fr = @user.friend_requests.select{| f | f.id == params[:id].to_i }

    if fr.size > 0
      fr_req = fr.first
      if(params[:status] == "confirm")
        fr_req.update_attribute(:status, "confirmed")
        Notification.create(:user_id => fr_req.user_id, :notifiable_type => "friend_user", :notifiable_id => fr_req.id, :notification_type => "friend_request_confirmed")
        flash.now[:notice] = "You are now friends with <a href='#{profile_user_url(fr_req.user)}'>#{fr_req.user.first_and_last_name}</a>"
      else
        notif = Notification.find_by_notification_type_and_notifiable_id("friend_requested", fr_req.id)
        notif.destroy if !notif.nil?
        fr_req.delete
        flash.now[:notice] = "The friend request from #{fr_req.user.first_and_last_name} has been denied"
      end
    end
    @requests = @user.friend_requests
    @friends = @user.confirmed_friends

    render :template => "friends/index"
  end
end