class UsersController < ApplicationController
  layout "application"
  require 'RMagick'
  require 'typhoeus'
  require 'json'

  before_filter :init, :only => [:edit, :update]

  def init
    if @user.nil?
      redirect_to login_index_url
    end
  end

  def new
  end

  def edit
  end

  def create
    @user = User.new( params[ :user ] )

    if @user.valid?
      @user.active = true
      @user.save
      @user.create_default_wallpinnings
      session[ :user_id ] = @user.id

      UserMailer.deliver_welcome_email(@user) if Constants::DeliverMail && EmailUnsubscribe.find_by_email_and_set(@user.email, true).nil?
      flash[ :notice ] = "Welcome to your main wall.  You can now post to your wall, <a href='#{find_friends_url}'>Find Friends</a>, or <a href='#{invite_users_url}'>Invite Friends</a>."
      redirect_to :controller => :wall, :action => :index
    else
      flash.now[ :error ] = Util.errors_with_markup( @user.errors )
      render :template => "users/new"
    end
  end

  def update
    @user.attributes = params[ :user ]
    @user.cancel_password_validation = true

    if @user.valid?
      @user.save()

      file_data = params.has_key?("profile") ? params[ "profile" ][ "photo" ] : nil
      if file_data

        file_ext = file_data.original_filename.split('.').last

        if [ "jpg", "jpeg", "png", "gif" ].include?( file_ext.downcase )
          photo_path = "#{RAILS_ROOT}/public/images/uploads/"
          if !@user.photo.nil?
            # delete the old photos
            File.delete( "#{photo_path}#{@user.photo.id}.#{@user.photo.file_ext}" ) if File.exists? "#{photo_path}#{@user.photo.id}.#{@user.photo.file_ext}"
            File.delete( "#{photo_path}#{@user.photo.id}_128x128.#{@user.photo.file_ext}" ) if File.exists? "#{photo_path}#{@user.photo.id}_128x128.#{@user.photo.file_ext}"
            File.delete( "#{photo_path}#{@user.photo.id}_64x64.#{@user.photo.file_ext}" ) if File.exists? "#{photo_path}#{@user.photo.id}_64x64.#{@user.photo.file_ext}"
            File.delete( "#{photo_path}#{@user.photo.id}_32x32.#{@user.photo.file_ext}" ) if File.exists? "#{photo_path}#{@user.photo.id}_32x32.#{@user.photo.file_ext}"
            File.delete( "#{photo_path}#{@user.photo.id}_15x15.#{@user.photo.file_ext}" ) if File.exists? "#{photo_path}#{@user.photo.id}_15x15.#{@user.photo.file_ext}"
            @user.photo.destroy()
          end

          # create image directory
          FileUtils.makedirs( photo_path )

          # create photo record
          new_photo = Photo.create( :file_ext => file_ext, :user_id => @user.id )

          # save image @ original size
          f = File.new("#{photo_path}#{new_photo.id}.#{new_photo.file_ext}", "wb")
          f.write file_data.read
          f.close

          # create thumbnails
          img = Magick::Image.read( "#{photo_path}#{new_photo.id}.#{new_photo.file_ext}" ).first
          img.resize_to_fit( 128, 128 ).write "#{photo_path}#{new_photo.id}_128x128.#{new_photo.file_ext}"
          img.resize_to_fit( 64, 64 ).write "#{photo_path}#{new_photo.id}_64x64.#{new_photo.file_ext}"
          img.resize_to_fit( 32, 32 ).write "#{photo_path}#{new_photo.id}_32x32.#{new_photo.file_ext}"
          img.resize_to_fit( 15, 15 ).write "#{photo_path}#{new_photo.id}_15x15.#{new_photo.file_ext}"
          flash.now[ :notice ] = "Your profile was updated"
        else
          flash.now[ :error ] = "Only files with extension (jpg, png, gif) are allowed"
        end
      else
        flash.now[ :notice ] = "Your profile was updated"
      end
    else
      flash.now[ :error ] = Util.errors_with_markup( @user.errors )
    end
    @user.reload
    render :template => "users/edit"
  end

  def destroy
    @user.destroy_profile

    reset_session
    cookies.delete( :remember_me_id ) if cookies[ :remember_me_id ]
    cookies.delete( :remember_me_code ) if cookies[ :remember_me_code ]
    flash[:notice] = "Your account was deleted.  We hope to see you again soon!"
    redirect_to root_url
  end

  def make_fb_friends(user, friends_list)
    pin_friends = []
    friends_list["data"].each do | friend |
      friend = User.find_by_facebook_id(friend["id"])
      if !friend.nil?
        user.become_friend(friend)
        pin_friends << friend if pin_friends.size < 3
      end
    end

    user.reload
    pin_friends.each_with_index do | friend, index |
      wp = user.pinning_in_slot(user.wall_pinnings, index)
      wp.pinned_user = friend
      wp.save
    end
  end

  def fb_register
    parsed_params = FacebookRegistration::SignedRequest.new.parse_params(params["signed_request"])

    valid = fb_params_valid?(parsed_params["registration_metadata"]["fields"])

    if valid
      user = User.new
      user.first_name = parsed_params["registration"]["first_name"]
      user.last_name = parsed_params["registration"]["last_name"]
      user.email = parsed_params["registration"]["email"]

      if parsed_params["registration"].has_key?("location") && parsed_params["registration"]["location"].has_key?("name") && parsed_params["registration"]["location"]["name"].include?(",")
        user.city = parsed_params["registration"]["location"]["name"].split(",")[0].strip
        user.state = parsed_params["registration"]["location"]["name"].split(",")[1].strip
      end

      user.password = parsed_params["registration"]["password"]
      user.password_confirmation = parsed_params["registration"]["password"]
      user.facebook_id = parsed_params["user_id"]

      if user.valid?
        user.active = true
        user.save
        user.create_default_wallpinnings

        if parsed_params['user_id'].present?
          me_url = "https://graph.facebook.com/me/friends?access_token=#{parsed_params['oauth_token']}"
          hydra = Typhoeus::Hydra.new
          request = Typhoeus::Request.new(me_url, :method => :get, :timeout => 10000)
          hydra.queue(request)
          hydra.run
          response = request.response
          friends_list = ActiveSupport::JSON.decode(response.body)

          make_fb_friends(user, friends_list)
        end

        session[ :user_id ] = user.id
        remember_user( user )

        UserMailer.deliver_welcome_email(user) if Constants::DeliverMail && EmailUnsubscribe.find_by_email_and_set(user.email, true).nil?
        flash[ :notice ] = "You are now registered.  <a href='#{find_friends_url}'>Find Friends</a> or <a href='javascript: void(0);' onclick='Groovesy.SendFBAppRequestViaMultiFriendSelector()'><img src='#{ActionController::Base.helpers.asset_path('fb_invite_button.png')}' border='0' align='absmiddle'/></a>"
        redirect_to :controller => :streams, :action => :index
        return
      else
        flash.now[ :error ] = Util.errors_with_markup( user.errors )
      end
    else
      flash.now[:error] = "There was a problem with the request"
    end
    render :template => "users/new"
  end

  def fb_params_valid?(params)
    params == "[{'name':'name', 'view':'prefilled'},{'name':'first_name'},{'name':'last_name'},{'name':'email'},{'name':'password'},{'name':'location'}]"
  end

  def fb_connect
    if params.has_key?("error_reason") && params["error_reason"] == "user_denied"
      flash.now[:notice] = "You denied the connection"
    else
      auth_url = "https://graph.facebook.com/oauth/access_token?client_id=#{Rails.configuration.FacebookAppId}&redirect_uri=#{fb_connect_user_url(@user)}&client_secret=#{Rails.configuration.FacebookAppSecret}&code=#{params['code']}"

      request = Typhoeus::Request.new(auth_url, :method => :get, :timeout => 10000)

      hydra = Typhoeus::Hydra.new
      hydra.queue(request)
      hydra.run
      response = request.response

      if response.code == "400"
        flash.now[:error] = "There was an error authenticating"
      else
        access_token = response.body.split("=")[1]
        me_url = "https://graph.facebook.com/me?access_token=#{access_token}"
        request = Typhoeus::Request.new(me_url, :method => :get, :timeout => 10000)
        hydra.queue(request)
        hydra.run
        response = request.response
        me = ActiveSupport::JSON.decode(response.body)

        #Rails.logger.info "\n\n#{response.body}\n\n"
        @user.update_attribute(:facebook_id, me["id"])

        flash.now[:notice] = "Facebook is now connected with your Groovesy account"
      end
    end
    render :template => "users/edit"
  end

  def ajax_find_friend
    term = params["term"].gsub('%', '\%').gsub('_', '\_')

    @users = @user.search_friends( term )
    respond_to do |format|
      format.json { render :json => @users.collect{ | u | {:label => u.first_and_last_name + " - " + u.email, :value => u.id } } }
    end
  end

  def invite
    @active_tab = "friends"
    render :template => "users/invite"
  end

  def send_invitation
    @active_tab = "friends"
    if !params[:invitation][:email].blank?
      emails = params[:invitation][:email].split(",")
      emails.each do | email |
        UserMailer.deliver_invite( @user, email ) if Constants::DeliverMail && EmailUnsubscribe.find_by_email_and_set(email, true).nil?
      end
      flash.now[:notice] = "Thanks, your invitation was sent"
    else
      flash.now[ :error ] = Util.errors_with_markup( "Email is required" )
    end
    
    render :template => "users/invite"
  end

  def profile
    @active_tab = "walls"
    @profile = User.find( params[ :id ] )

    wall_pinnings = @profile.wall_pinnings.all(:include => {:pinned_user => [:wall_posts, :photo]}, :order => "slot ASC")
    @wall_configs = []
    wall_pinnings.each do | wp |
      if wp.slot == 0
        @wall_configs << WallConfig.new({:pinning => wp,
                                         :on_your_page => false })
      else
        @wall_configs << WallConfig.new({:pinning => wp,
                                         :on_your_page => false })
      end
    end

    @profile_friends = @profile.confirmed_friends

    @wall_post = WallPost.new
    render :layout => "full_page"
  end

  def wall
    redirect_to :action => :profile
  end
end