class LoginController < ApplicationController
  layout "application"

  def index
  end

  def create
    session[ :user_id ] = nil
    user = User.authenticate( params[ :login ][ :email ], params[ :login ][ :password ] )
    if user
      session[ :user_id ] = user.id
      remember_user( user ) if params[ :login ][ :remember_me ] == "true"
      authenticate
      load_wall
      return
    else
      flash.now[ :error ] = Util.errors_with_markup( "Invalid user/password combination.  Please try again." )
      render :template => "login/index"
    end
  end

  def fb_login
    if params.has_key?("error_reason") && params["error_reason"] == "user_denied"
      flash.now[:notice] = "Would you like to login without Facebook?"
    else
      result = Facebook.login(params['code'], fb_login_login_index_url)

      if result[:success]
        @user = User.find_by_email_and_facebook_id(result[:me]["email"], result[:me]["id"])

        if !@user.nil?
          session[ :user_id ] = @user.id
          remember_user( @user )
          authenticate
          load_wall
          return
        else
          flash.now[:error] = "We didn't find you in our system, or you have not connected your Groovesy account with Facebook.<br /><br />If you've already registered...<br /><p style='text-indent: 25px;'>Log in with Groovesy and click 'Profile' to link Facebook with your Groovesy account.</p><br />Otherwise,<br /><p style='text-indent: 25px;'><a href='#{new_user_url}'>Register here</a></p>"
        end
      else
        flash.now[:error] = "There was an error authenticating."
      end
    end
    render :template => "login/index"
  end

  def destroy
    reset_session
    cookies.delete( :remember_me_id ) if cookies[ :remember_me_id ]
    cookies.delete( :remember_me_code ) if cookies[ :remember_me_code ]
    redirect_to :action => :index
  end

  def retrieve_password
  end

  def mail_guid
    user = User.find_by_email( params[ :email ] )
    if !user.nil?
      user.update_attribute(:guid, User.generate_new_guid)
      UserMailer.deliver_retrieve_password( user )
      flash.now[:notice] = "A link has been sent to your email address to change your password."
    else
      flash.now[:error] = "Email not found.  Please try again."
    end
    render :template => "login/retrieve_password"
  end

  def reset_password
    @user = User.find_by_guid( params[ :guid ] ) if params[:guid].present?
    if @user.nil?
      flash.now[:notice] = "The link is invalid.  Please re-click the link in the email you received, or copy it and paste it into the address bar."
    end
  end

  def save_password
    @user = User.find_by_guid( params[ :guid ] ) if params[:guid].present?
    if @user.nil?
      flash.now[:notice] = "The information you provided is invalid."
      render :template => "login/reset_password"
    else
      @user.attributes = params[:user]
      if @user.valid?
        @user.save
        flash[:notice] = "Your password has been reset."
        session[ :user_id ] = @user.id
        redirect_to streams_url
      else
        flash.now[:error] = Util.errors_with_markup( @user.errors )
        render :template => "login/reset_password"
      end
    end
  end

  def load_wall
    @active_tab = "walls"
    @wall_post = WallPost.new
    @posts = @user.posts
    render :template => "wall/index", :layout => "full_page_isotope"
  end
end