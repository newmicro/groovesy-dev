class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :authenticate
  
  #rescue_from Exception, :with => :rescue_all_exceptions if RAILS_ENV == 'production'

  def authenticate
    # check session
    if !session[ :user_id ].nil?
      @user = User.find( session[ :user_id ] )
    elsif cookies[ :remember_me_id ] && cookies[ :remember_me_code ] # check cookie

      u = User.find( cookies[ :remember_me_id ] )
      if u && Digest::SHA1.hexdigest( User.find( cookies[ :remember_me_id ] ).email )[4,18] == cookies[ :remember_me_code ]
        @user =  u
        session[ :user_id ] = @user.id
      end
    end
  end

  def remember_user( user )
    cookies[ :remember_me_id ] = { :value => user.id.to_s, :expires => 30.days.from_now }  
    cookies[ :remember_me_code ] = { :value => Digest::SHA1.hexdigest( user.email )[4,18], :expires => 30.days.from_now }  
  end
=begin
  def rescue_all_exceptions( exception )
    case exception
      when ActiveRecord::RecordNotFound
        AdminMailer.deliver_error_message( request, exception, session )
        render :template => "shared/error", :locals => { :error => "<br /><br />Error: <span id='error'>Record not found</span>" }, :status => :not_found, :layout => false
      when ActionController::RoutingError, ActionController::UnknownController, ActionController::UnknownAction
        AdminMailer.deliver_error_message( request, exception, session )
        render :template => "shared/error", :locals => { :error => "<br /><br />Error: <span id='error'>Page not found.  Please check the url and try again.</span>" }, :status => :not_found, :layout => false
      else
        error = "\nWhile processing a #{request.method} request on #{request.path}\nparameters: #{request.parameters}\n#{exception.message}\n#{exception.clean_backtrace.join( '\n' )}\n\n"
        logger.error( error )
        AdminMailer.deliver_error_message( request, exception, session )
        render :template => "shared/error", :locals => { :error => "" }, :layout => false, :status => :internal_server_error
    end
  end
=end
end