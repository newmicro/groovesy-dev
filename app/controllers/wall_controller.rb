class WallController < ApplicationController
  layout "application"
  before_filter :init, :except => [:view]
  helper :wall

  def init
    @active_tab = "walls"
    if @user.nil?
      redirect_to login_index_url
    end
  end

  def index
    @wall_post = WallPost.new
    @posts = @user.posts
    render :layout => "full_page_isotope"
  end

  def friends
    @posts = @user.friend_posts
    render :layout => "full_page_isotope"
  end

  def my_likes
    @posts = @user.likes
    render :layout => "full_page_isotope"
  end

  def new
    @wall = Wall.new
  end

  def create
    @wall = Wall.new(params[:wall])
    @wall.set_slug
    @wall.admin_id = @user.id
    if @wall.valid?
      @wall.save
      @user.join_wall(@wall)
      @posts = @wall.posts.all(:include => [:likes, {:comments => {:user => :photo}}], :order => "created_at DESC", :limit => Constants::MaxPostsPublicWall)
      @wall_post = WallPost.new(:wall_id => @wall.id)
      @user.reload
      flash.now[:notice] = "Your wall has been created, and you are the admin."
      render :template => "wall/view", :layout => "wall_with_right_sidebar"
    else
      flash.now[:error] = Util.errors_with_markup( @wall.errors )
      render :template => "wall/new"
    end
  end

  def edit
    @wall = Wall.find(params[:id])
  end

  def update
    @wall = Wall.find(params[:id])
    @wall.attributes = params[:wall]
    @wall.set_slug
    if @wall.valid?
      @wall.save
      flash[:notice] = "The wall has been updated."
      redirect_to view_wall_url(@wall.slug)
    else
      flash.now[:error] = Util.errors_with_markup( @wall.errors )
      render :template => "wall/edit"
    end
  end

  def view
    @active_tab = "walls"
    @wall = Wall.first(:conditions => ["slug = ?", params[:slug]])
    @wall_post = WallPost.new(:wall_id => @wall.id)
    @posts = @wall.posts.all(:include => [:likes, {:comments => {:user => :photo}}], :order => "created_at DESC", :limit => Constants::MaxPostsPublicWall)
    render :layout => "wall_with_right_sidebar"
  end

  def destroy
    @wall = Wall.find_by_id_and_admin_id(params[:id], @user.id)
    if !@wall.nil?
      @wall.posts.each{ | p | p.delete_post }
      @wall.wall_members.each{ |wm| wm.destroy }
      @wall.destroy
      @walls = @user.admin_walls
      @random_walls = Wall.random_walls
      render :template => "wall/my_walls"
      return
    end
    render :text => ""
  end

  def search
    @walls = Wall.search(params[:tag])
  end

  def my_walls
    @walls = @user.admin_walls
    @random_walls = Wall.random_walls
    render :layout => "full_page"
  end

  def unsubscribe
    @wall = Wall.find(params[:id])
    if @user.member_of?(@wall)
      @user.leave_wall(@wall)
      flash.now[:notice] = "You are unsubscribed from #{@wall.name}"
    else
      flash.now[:error] = "You are not subscribed to #{@wall.name}"
    end
    @user.reload
    @walls = @user.admin_walls
    @random_walls = Wall.random_walls
    render :template => "wall/my_walls"
  end

  def ajax_subscribe
    @wall = Wall.find(params[:id])
    if !@user.member_of?(@wall)
      @user.join_wall(@wall)
      respond_to do |format|
        format.js{ render "wall/ajax/ajax_subscribe" }
      end
      return
    end
    render :text => ""
  end

  def ajax_unsubscribe
    @wall = Wall.find(params[:id])
    if @user.member_of?(@wall)
      @user.leave_wall(@wall)
      respond_to do |format|
        format.js{ render "wall/ajax/ajax_unsubscribe" }
      end
      return
    end
    render :text => ""
  end

  def ajax_post
    @wall_post = WallPost.new(params[:wall_post])
    @wall_post.post_type = "user_wall"
    @wall_post.user = @user
    @wall_post.poster_user = @user
    Util.strip_html(@wall_post.post_text)
    if @wall_post.valid?
      @wall_post.get_track_details
      @wall_post.save
      respond_to do |format|
        format.js{ render "wall/ajax/ajax_post" }
      end
    else
      render :text => ""
    end
  end

  def ajax_public_post
    @wall_post = WallPost.new(params[:wall_post])
    @wall_post.post_type = "public_wall"
    @wall_post.poster_user = @user
    Util.strip_html(@wall_post.post_text)
    if @wall_post.valid?
      @wall_post.get_track_details
      @wall_post.save
      @wall_post.wall.wall_users.each do | u |
        if u != @user && Constants::DeliverMail && EmailUnsubscribe.find_by_email_and_set(u.email, true).nil?
          UserMailer.deliver_public_wall_post_notification( u, @wall_post.wall, @user, @wall_post )
        end
      end
      respond_to do |format|
        format.js{ render "wall/ajax/public/ajax_post" }
      end
    else
      render :text => ""
    end
  end

  def ajax_load_post_details
    wall_post = WallPost.new({:post_text => params['post_text']})
    wall_post.get_track_details

    respond_to do |format|
      format.json { render :json => wall_post.preview_json }
    end
  end
end