class FriendsController < ApplicationController
  layout "application"
  before_filter :init

  def init
    @active_tab = "friends"
  end

  def index
    get_friends
    @user.acknowledge_new_friends
  end

  def find
  end

  def search
    @results = User.search_friend( params[:friend], @user )
  end

  def destroy
    @user.remove_friendship(params[:id])
    
    flash.now[:notice] = "#{User.find(params[:id]).first_and_last_name} was removed from your friends list"
    get_friends
    render :template => "friends/index"
  end

  private

  def get_friends
    @requests = @user.friend_requests
    @friends = @user.confirmed_friends.sort{|a,b| a.first_name <=> b.first_name }
  end
end