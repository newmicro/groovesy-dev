var Groovesy = (function() {

  var colorAnimColors;
  var colorAnimTarget;
  var colorAnimTimer;
  var bgColorAnimColors;
  var bgColorAnimTarget;
  var bgColorAnimTimer;
  var textAreaPlaceholderTxt = "SoundCloud, YouTube, Mixcloud, Image link...";

  var initialize = function() {

    colorAnimColors = new Array("#bb8ca1", "#939ea3", "#857c9d", "#be9595");
    colorAnimTarget = null;
    colorAnimTimer = null;
    bgColorAnimColors = new Array("#bb8ca1", "#939ea3", "#857c9d", "#be9595", "#000000");
    bgColorAnimTarget = null;
    bgColorAnimTimer = null;

    mouse_is_inside = false;
    notif_mouse_is_inside = false;

    $(".add_comment_link").bind("click", AddCommentClicked);
    $("#share_post").bind("click", ShowShareLinks);
    $("#notice_close_btn").bind("click", CloseNotice);
    $("#error_close_btn").bind("click", CloseNotice);
    $("input[type=submit].post").bind("click", ValidatePost);
    $(".wall_post_comment_form").submit(AppendCommentPageNumber)
    //SetupPostTags();
    $("#logo_video").bind('ended', RestartLogoVideo);

    SetupLikesHide();

    $(".post_preview_close_button").bind("click", function(e){
      ClosePostPreview($(e.currentTarget).attr("data-id"));
    });

    $(".public_wall_input_box, .wall_input_box").bind('paste', function(e) {
      setTimeout(function() {
        PostTextPasted(e);
      }, 100);
    });

    $(document).mousemove(function(e) {
      mouseX = e.pageX;
      mouseY = e.pageY;
      //console.log(mouseX + ":" + mouseY);
    });

    SetupNavColorAnimation();

    if(!Modernizr.input.placeholder)
    {
      $('[placeholder]').focus(function() {
        var input = $(this);
        if (input.val() == input.attr('placeholder')) {
        input.val('');
        input.removeClass('placeholder');
        }
      }).blur(function() {
        var input = $(this);
        if (input.val() == '' || input.val() == input.attr('placeholder')) {
        input.addClass('placeholder');
        input.val(input.attr('placeholder'));
        }
      }).blur();
      $('[placeholder]').parents('form').submit(function() {
        $(this).find('[placeholder]').each(function() {
        var input = $(this);
        if (input.val() == input.attr('placeholder')) {
          input.val('');
        }
        })
      });
    }
  }

  var HideLikes = function(e) {
    var container = $("#likes_list");

    if (container.has(e.target).length === 0)
    {
      container.hide();
    }
  }

  var SetupLikesHide = function() {
    $(document).mouseup(HideLikes);
    $("body").bind( "touchend", HideLikes);
  }

  var RestartLogoVideo = function () {
    this.currentTime = 0.1; //setting to zero breaks iOS 3.2, the value won't update, values smaller than 0.1 was causing bug as well.
    this.play();
  }

  var AppendCommentPageNumber = function(e) {
    var post_id = $(e.currentTarget).attr("data-id");
    $("body").find("[data-id='" + post_id + "'] .wall_post_comment_form [name='page']").val(window["current_comments_page_" + post_id ]);
    return true;
  }

  var SetupPostTags = function() {
    $( "#_tags" )
      // don't navigate away from the field on tab when selecting an item
      .bind( "keydown", function( event ) {
        if ( event.keyCode === $.ui.keyCode.TAB &&
          $( this ).data( "autocomplete" ).menu.active ) {
          event.preventDefault();
        }
      })
      .autocomplete({
        source: function( request, response ) {
          $.getJSON( "search.php", {
            term: extractLast( request.term )
          }, response );
        },
        search: function() {
          // custom minLength
          var term = extractLast( this.value );
          if ( term.length < 2 ) {
            return false;
          }
        },
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function( event, ui ) {
          var terms = split( this.value );
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.value );
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          this.value = terms.join( ", " );
          return false;
        }
      });
  }

  var PostTextDataReceived = function(data, data_id) {
    $("#preview_ajax_loader_" + data_id).hide();

    if(data.provider == "Unknown") {
      ClosePostPreview(data_id);
    }
    else {
      var params = { url: data.url, thumbnail_url: data.thumbnail_url, title: data.title, artist: data.artist, provider: data.provider };

      $( "#" + data.provider.toLowerCase() + "_tmpl" ).tmpl( params ).appendTo( "#post_preview_media_container_" + data_id);

      Groovesy.MakeColorbox();
    }
  }

  var split = function( val ) {
    return val.split( /,\s*/ );
  }

  var extractLast = function( term ) {
    return split( term ).pop();
  }

  var PostTextPasted = function(e) {
    var data_id = $(e.currentTarget).attr("data-id");

    if ($(e.currentTarget).val().match(/https?:\/\//)) {
      $("#preview_ajax_loader_" + data_id).show();
      $("#post_preview_" + data_id).slideDown();
      $("#post_preview_media_container_" + data_id).empty();

      $.ajax({
        type: "GET",
        url: "/wall/ajax_load_post_details",
        data: { post_text: $(e.currentTarget).val() },
        success: function(data) {
          PostTextDataReceived(data, data_id);
        }
      });
    }
  }

  var ClosePostPreview = function(data_id) {
    $("#preview_ajax_loader_" + data_id).hide();
    $("#post_preview_" + data_id).slideUp();
    $("#post_preview_media_container_" + data_id).empty();
  }

  var ValidatePost = function(e) {
    var textBox;
    var dataId = $(e.currentTarget).attr("data-id");
    $(".wall_input_box, .public_wall_input_box").each(function(){
      if($(this).attr("data-id") == dataId) {
        textBox = this;
      }
    });

    if($(textBox).val() != "" && $(textBox).val() != textAreaPlaceholderTxt) {
      $("body").find("[data-id='" + dataId + "'].input_box_mask").show();
      return true;
    }
    else {
      return false;
    }
  }

  var CloseNotice  = function() {
    if($(this).attr("id") == "notice_close_btn"){
      $("#flashNotice").slideUp("slow");
    }
    else {
      $("#flashError").slideUp("slow");
    }
  }

  var UrlQuery = function( query ) {
  	query = query.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  	var expr = "[\\?&]"+query+"=([^&#]*)";
  	var regex = new RegExp( expr );
  	var results = regex.exec( window.location.href );
  	if( results !== null ) {
  		return results[1];
  		return decodeURIComponent(results[1].replace(/\+/g, " "));
  	} else {
  		return false;
  	}
  }

  var DeleteFBAppRequest = function() {
    var request_ids = UrlQuery("request_ids");
    var ids = request_ids.split(",");

    for(var i = 0; i < ids.length; i++) {
      FB.api(ids[ i ], 'delete', function(response) {
        //console.log(response);
      });
    }
  }

  var SendFBAppRequestViaMultiFriendSelector = function() {
    FB.ui({method: 'apprequests',
      message: 'Groovesy Invitation',
      redirect_uri: 'http://www.groovesy.com/facebook/invited'
    }, null);
  }

  var ShowShareLinks = function() {
    $("#close_button").bind("click", HideShareLinks);
    $("#links").slideDown();
    $("#share_post").hide();
    $("#close_button").show();
  }

  var HideShareLinks = function() {
    $("#close_button").hide();
    $("#links").slideUp();
    $("#share_post").fadeIn();
  }

  var FriendSelected = function(event, ui) {
    var html = "<span>" + ui.item.label + "</span>" + "<div id='close_button_new'></div>";

    $("#find_friend").attr("disabled", "disabled");

    $("#found_friend").show();
    $("#found_friend").html(html);

    $("#request_friend_form").find("#wall_pinning_pinned_user_id").val(ui.item.value);

    $("#request_friend_form").find("input[type=submit]").removeAttr("disabled");

    $("#close_button_new").bind("click", function(){
      $("#request_friend_form").find("#wall_pinning_pinned_user_id").val(null);
      $("#found_friend").html("");
      $("#request_friend_form").find("input[type=submit]").attr("disabled", "disabled");
      $("#find_friend").removeAttr("disabled");
      $("#find_friend").val("");
      $("#found_friend").hide();
    });
  }

  var ShowMorePosts = function(wp_id) {
    window["current_page_" + wp_id ] = window["current_page_" + wp_id ] + 1;

    $.ajax({
      type: "GET",
      url: "/streams/ajax_load_posts",
      data: { wall_pinning_id: wp_id, page: window["current_page_" + wp_id ] }
    });
  }

  var ShowMoreComments = function(post_id, url, render_context) {
    window["current_comments_page_" + post_id ]++;

    $.ajax({
      type: "GET",
      url: url,
      data: { post_id: post_id, render_context: render_context, page: window["current_comments_page_" + post_id ] }
    });
  }

  var AddCommentClicked = function(e) {
    post_id = $(e.currentTarget).attr("data-id");

    $("body").find("[data-id='" + post_id + "'].add_comment_link").hide();
    $("body").find("[data-id='" + post_id + "'] form").show();
    $("body").find("[data-id='" + post_id + "'] .more").hide();
    if($("body").attr("data-type") == "isotope") {
      ReIsotope("post");
    }
  }

  var CloseRepinForm = function() {
    $("#repin_form").fadeOut("slow");
  }

  var ReIsotope = function(selector) {
    $('#posts_container').isotope({
      itemSelector : '.' + selector,
      transformsEnabled: false
    });
  }

  var FBShare = function(name, link, description) {
    FB.ui({
      method: 'feed',
      name: name,
      link: link,
      picture: 'http://www.groovesy.com/images/logo.jpg',
      description: description,
      message: ""
    });
  }

  var FBRichShare = function(name, link, thumbnail, source, description) {
    var fb_post = {
      "method":"feed",
      "name": name,
      "link": link,
      "picture": thumbnail,
      "source": source,
      "description": description,
      "actions":[{"name":"Discover and share the best tracks @ Groovesy.com",
                  "link":"http://www.groovesy.com/"}]
    };
    FB.ui(fb_post);
  }

  var MakeColorbox = function() {
    $(".colorboxvid, .colorboximg").each(function() {
      if($(this).attr("data-provider") == "Youtube" || $(this).attr("data-provider") == "Image") {
        var height = "75%";
      }
      else {
        var height = "256px";
      }
      $(this).colorbox({
        iframe: true,
        width: "75%",
        height: height,
        title: function(){ return $(this).attr("like-html"); }
      });
    });
  }

  var AnimateFonts = function() {
    if(colorAnimTarget == null){
      clearTimeout(colorAnimTimer);
      return;
    }

    var rand = Math.floor((Math.random()*4));
    $(colorAnimTarget).animate({color: colorAnimColors[rand]});
    colorAnimTimer = setTimeout(AnimateFonts, 2000);
  }

  var AnimateBG = function() {
    if(bgColorAnimTarget == null){
      clearTimeout(bgColorAnimTimer);
      return;
    }

    var rand = Math.floor((Math.random()*5));
    $(bgColorAnimTarget).animate({backgroundColor: bgColorAnimColors[rand]});
    bgColorAnimTimer = setTimeout(AnimateBG, 2000);
  }

  var SetupNavColorAnimation = function() {
    $("#walls_tab").mouseover(function() {colorAnimTarget = $("#walls_tab"); AnimateFonts();});
    $("#walls_tab").mouseout(function() {colorAnimTarget = null;});

    $("#streams_tab").mouseover(function() {colorAnimTarget = $("#streams_tab"); AnimateFonts();});
    $("#streams_tab").mouseout(function() {colorAnimTarget = null;});

    $("#friends_tab").mouseover(function() {colorAnimTarget = $("#friends_tab"); AnimateFonts();});
    $("#friends_tab").mouseout(function() {colorAnimTarget = null;});

    $("#notifications_tab").mouseover(function() {colorAnimTarget = $("#notifications_tab"); AnimateFonts();});
    $("#notifications_tab").mouseout(function() {colorAnimTarget = null;});

    $("#login_tab").mouseover(function() {colorAnimTarget = $("#login_tab"); AnimateFonts();});
    $("#login_tab").mouseout(function() {colorAnimTarget = null;});

    $(".more").mouseover(function(e) {bgColorAnimTarget = $(e.currentTarget); AnimateBG();});
    $(".more").mouseout(function(e) {bgColorAnimTarget = null;});
  }

  return { initialize:initialize,
           FriendSelected:FriendSelected,
           ShowMorePosts:ShowMorePosts,
           ShowMoreComments:ShowMoreComments,
           AddCommentClicked:AddCommentClicked,
           CloseRepinForm:CloseRepinForm,
           ReIsotope:ReIsotope,
           MakeColorbox:MakeColorbox,
           FBShare:FBShare,
           FBRichShare:FBRichShare,
           SendFBAppRequestViaMultiFriendSelector:SendFBAppRequestViaMultiFriendSelector,
           DeleteFBAppRequest:DeleteFBAppRequest,
           ClosePostPreview:ClosePostPreview};
})();

$(document).ready(function(){
  Groovesy.initialize();
});