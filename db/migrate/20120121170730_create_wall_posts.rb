class CreateWallPosts < ActiveRecord::Migration
  def self.up
    create_table :wall_posts do |t|
      t.column :user_id, :integer, :null => false
      t.column :poster_user_id, :integer, :null => false
      t.column :post_text, :text, :null => true
      t.column :post_embed_code, :text, :null => true
      t.timestamps
    end
  end

  def self.down
    drop_table :wall_posts
  end
end
