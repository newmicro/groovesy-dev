class CreateFriends < ActiveRecord::Migration
  def self.up
    create_table :friend_users, :id => false do |t|
      t.column :user_id, :integer, :null => false
      t.column :friend_user_id, :integer, :null => false
    end
  end

  def self.down
    drop_table :friends
  end
end
