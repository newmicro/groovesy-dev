class RemovePostEmbedCode < ActiveRecord::Migration
  def self.up
    remove_column :wall_posts, :post_embed_code
  end

  def self.down
    add_column :wall_posts, :post_embed_code, :text, :null => true
  end
end
