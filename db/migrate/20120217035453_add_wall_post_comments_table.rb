class AddWallPostCommentsTable < ActiveRecord::Migration
  def self.up
    create_table :wall_post_comments do | t |
      t.column "wall_post_id", :integer, :null => false
      t.column "commenter_id", :integer, :null => false
      t.column "comment", :text, :null => false
      t.timestamps
    end
  end

  def self.down
    drop_table :wall_post_comments
  end
end
