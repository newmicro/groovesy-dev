class AddWallMembersTable < ActiveRecord::Migration
  def self.up
    create_table :wall_members do |t|
      t.column :wall_id, :integer, :null => false
      t.column :user_id, :integer, :null => false
      t.timestamps
    end
  end

  def self.down
    drop_table :wall_members
  end
end
