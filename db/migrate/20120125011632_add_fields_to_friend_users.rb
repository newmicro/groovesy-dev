class AddFieldsToFriendUsers < ActiveRecord::Migration
  def self.up
    add_column :friend_users, :status, :string, :limit => 40, :null => true
  end

  def self.down
    remove_column :friend_users, :status
  end
end
