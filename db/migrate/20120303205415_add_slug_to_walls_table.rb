class AddSlugToWallsTable < ActiveRecord::Migration
  def self.up
    add_column :walls, :slug, :string, :null => false
  end

  def self.down
    remove_column :walls, :slug
  end
end
