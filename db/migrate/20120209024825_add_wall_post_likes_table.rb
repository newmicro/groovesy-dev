class AddWallPostLikesTable < ActiveRecord::Migration
  def self.up
    create_table :wall_post_likes do | t |
      t.column "wall_post_id", :integer, :null => false
      t.column "liker_id", :integer, :null => false
      t.timestamps
    end
  end

  def self.down
    drop_table :wall_post_likes
  end
end
