class AddWallPostDetails < ActiveRecord::Migration
  def self.up
    add_column :wall_posts, :thumbnail_url, :string, :null => true
    add_column :wall_posts, :artist, :string, :null => true
    add_column :wall_posts, :title, :string, :null => true
    add_column :wall_posts, :description, :text, :null => true
    add_column :wall_posts, :api_provider, :string, :null => true
    add_column :wall_posts, :meta_title, :string, :null => true
    add_column :wall_posts, :is_video, :boolean, :default => false
  end

  def self.down
    remove_column :wall_posts, :artist
    remove_column :wall_posts, :title
    remove_column :wall_posts, :description
    remove_column :wall_posts, :api_provider
    remove_column :wall_posts, :meta_title
    remove_column :wall_posts, :is_video
    remove_column :wall_posts, :thumbnail_url
  end
end
