class AddTagsToWalls < ActiveRecord::Migration
  def self.up
    add_column :walls, :tags, :string, :null => true
  end

  def self.down
    remove_column :walls, :tags
  end
end
