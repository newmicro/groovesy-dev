class AddLocationToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :city, :string, :null => true
    add_column :users, :state, :string, :null => true
  end

  def self.down
    remove_column :users, :city
    remove_column :users, :state
  end
end