class AddPhotosTable < ActiveRecord::Migration
  def self.up
    create_table :photos do | t |
      t.column "user_id", :int, :null => false
      t.column "file_ext", :string, :null => false, :limit => 5
      t.timestamps
    end
  end

  def self.down
    drop_table :photos
  end
end
