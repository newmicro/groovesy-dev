class AddAcknowledgedToFriendUsers < ActiveRecord::Migration
  def self.up
    add_column :friend_users, :acknowledged, :boolean, :default => false
  end

  def self.down
    remove_column :friend_users, :acknowledged
  end
end
