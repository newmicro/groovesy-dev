class AddRepinParentId < ActiveRecord::Migration
  def self.up
    add_column :wall_posts, :repin_parent_id, :integer, :null => true
  end

  def self.down
    remove_column :wall_posts, :repin_parent_id
  end
end
