class CreateUsers < ActiveRecord::Migration
  def self.up
    create_table :users do |t|
      t.column "nickname", :string, :null => true, :limit => 50
      t.column "first_name", :string, :null => true, :limit => 25
      t.column "last_name", :string, :null => true, :limit => 25
      t.column "email", :string, :null => false, :limit => 65
      t.column "password_hash", :string, :null => false, :limit => 40
      t.column "salt", :string, :null => false, :limit => 256
      t.column "guid", :string, :null => true, :limit => 36
      t.timestamps
    end
  end

  def self.down
    drop_table :users
  end
end
