class RemoveNickname < ActiveRecord::Migration
  def self.up
    remove_column :users, :nickname
  end

  def self.down
    add_column :users, :nickname, :string, :null => true, :limit => 50
  end
end
