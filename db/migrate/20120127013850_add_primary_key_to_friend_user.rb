class AddPrimaryKeyToFriendUser < ActiveRecord::Migration
  def self.up
    add_column :friend_users, :id, :primary_key
  end

  def self.down
    remove_column :friend_users, :id
  end
end
