class AddSelfWallPinning < ActiveRecord::Migration
  def self.up
    users = User.all
    users.each do | u |
      u.wall_pinnings.each do | wp |
        wp.update_attribute(:slot, wp.slot + 1)
      end
      WallPinning.create(:user_id => u.id, :slot => 0, :pinned_user_id => u.id)
    end
  end

  def self.down
  end
end
