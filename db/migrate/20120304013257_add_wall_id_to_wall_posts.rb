class AddWallIdToWallPosts < ActiveRecord::Migration
  def self.up
    add_column :wall_posts, :wall_id, :integer, :null => true
    add_column :wall_posts, :post_type, :string, :null => false
    WallPost.update_all("post_type='user_wall'")
  end

  def self.down
    remove_column :wall_posts, :wall_id
    remove_column :wall_posts, :post_type
  end
end
