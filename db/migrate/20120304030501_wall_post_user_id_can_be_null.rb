class WallPostUserIdCanBeNull < ActiveRecord::Migration
  def self.up
    change_column :wall_posts, :user_id, :integer, :null => true
  end

  def self.down
    change_column :wall_posts, :user_id, :integer, :null => false
  end
end
