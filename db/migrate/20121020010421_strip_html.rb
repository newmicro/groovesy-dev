class StripHtml < ActiveRecord::Migration
  def self.up
    wps = WallPost.all
    wps.each do |wp|
      wp.post_text = Util.strip_html(wp.post_text)
      wp.save
    end
  end

  def self.down
  end
end
