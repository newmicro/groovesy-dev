class AddNotificationsTable < ActiveRecord::Migration
  def self.up
    create_table :notifications do | t |
      t.column :user_id, :integer, :null => false
      t.column :notifiable_id, :integer, :null => false
      t.column :notifiable_type, :string, :null => false
      t.column :notification_type, :string, :null => false
      t.column :acknowledged, :boolean, :default => false
      t.timestamps
    end
  end

  def self.down
    drop_table :notifications
  end
end