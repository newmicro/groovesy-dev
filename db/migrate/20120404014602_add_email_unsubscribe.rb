class AddEmailUnsubscribe < ActiveRecord::Migration
  def self.up
    create_table :email_unsubscribes do | t |
      t.column "email", :string, :null => false, :limit => 255
      t.column "key", :string, :null => false, :limit => 255
      t.column "set", :boolean, :default => false
      t.timestamps
    end
  end

  def self.down
    drop_table :email_unsubscribes
  end
end
