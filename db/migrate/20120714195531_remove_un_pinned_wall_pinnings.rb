class RemoveUnPinnedWallPinnings < ActiveRecord::Migration
  def self.up
    WallPinning.delete_all("pinned_user_id IS NULL")
    users = User.all
    users.each do | u |
      wps = u.wall_pinnings.order("slot ASC")
      i = 0
      wps.each do | wp |
        wp.update_attribute(:slot, i)
        i += 1
      end
    end
  end

  def self.down
  end
end
