class AddWallsTable < ActiveRecord::Migration
  def self.up
    create_table :walls do |t|
      t.column :name, :string, :null => false
      t.column :description, :text, :null => true
      t.column :admin_id, :integer, :null => false
      t.column :photo_id, :integer, :null => true
      t.timestamps
    end
  end

  def self.down
    drop_table :walls
  end
end
