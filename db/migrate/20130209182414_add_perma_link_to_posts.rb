class AddPermaLinkToPosts < ActiveRecord::Migration
  def self.up
    add_column :wall_posts, :provider_perma_link, :string, :null => true
    wps = WallPost.all
    wps.each do |wp|
      wp.get_track_details
      wp.save
    end
  end

  def self.down
    remove_column :wall_posts, :provider_perma_link
  end
end
