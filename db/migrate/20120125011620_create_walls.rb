class CreateWalls < ActiveRecord::Migration
  def self.up
    create_table :wall_pinnings do |t|
      t.column "user_id", :integer, :null => false
      t.column "slot", :integer, :null => false
      t.column "pinned_user_id", :integer, :null => true
      t.timestamps
    end
  end

  def self.down
    drop_table :wall_pinnings
  end
end
