class PopulateWallPosts < ActiveRecord::Migration
  def self.up
    wps = WallPost.all
    wps.each do |wp|
      wp.get_track_details
      wp.save
    end
  end

  def self.down

  end
end
