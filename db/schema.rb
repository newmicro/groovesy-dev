# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130209182414) do

  create_table "email_unsubscribes", :force => true do |t|
    t.string   "email",                         :null => false
    t.string   "key",                           :null => false
    t.boolean  "set",        :default => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "friend_users", :force => true do |t|
    t.integer "user_id",                                         :null => false
    t.integer "friend_user_id",                                  :null => false
    t.string  "status",         :limit => 40
    t.boolean "acknowledged",                 :default => false
  end

  create_table "notifications", :force => true do |t|
    t.integer  "user_id",                              :null => false
    t.integer  "notifiable_id",                        :null => false
    t.string   "notifiable_type",                      :null => false
    t.string   "notification_type",                    :null => false
    t.boolean  "acknowledged",      :default => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "photos", :force => true do |t|
    t.integer  "user_id",                 :null => false
    t.string   "file_ext",   :limit => 5, :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", :force => true do |t|
    t.string   "first_name",    :limit => 25
    t.string   "last_name",     :limit => 25
    t.string   "email",         :limit => 65,                    :null => false
    t.string   "password_hash", :limit => 40,                    :null => false
    t.string   "salt",          :limit => 256,                   :null => false
    t.string   "guid",          :limit => 36
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "zip"
    t.string   "facebook_id"
    t.boolean  "active",                       :default => true
    t.string   "city"
    t.string   "state"
  end

  create_table "wall_members", :force => true do |t|
    t.integer  "wall_id",    :null => false
    t.integer  "user_id",    :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "wall_pinnings", :force => true do |t|
    t.integer  "user_id",        :null => false
    t.integer  "slot",           :null => false
    t.integer  "pinned_user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "wall_post_comments", :force => true do |t|
    t.integer  "wall_post_id", :null => false
    t.integer  "commenter_id", :null => false
    t.text     "comment",      :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "wall_post_likes", :force => true do |t|
    t.integer  "wall_post_id", :null => false
    t.integer  "liker_id",     :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "wall_posts", :force => true do |t|
    t.integer  "user_id"
    t.integer  "poster_user_id",                         :null => false
    t.text     "post_text"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "repin_parent_id"
    t.integer  "wall_id"
    t.string   "post_type",                              :null => false
    t.string   "artist"
    t.string   "title"
    t.text     "description"
    t.string   "api_provider"
    t.string   "meta_title"
    t.boolean  "is_video",            :default => false
    t.string   "thumbnail_url"
    t.string   "provider_perma_link"
  end

  create_table "walls", :force => true do |t|
    t.string   "name",        :null => false
    t.text     "description"
    t.integer  "admin_id",    :null => false
    t.integer  "photo_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug",        :null => false
    t.string   "tags"
  end

end
