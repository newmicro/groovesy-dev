class Facebook
  def self.login(code, redirect_url)
    auth_url = "https://graph.facebook.com/oauth/access_token?client_id=#{Rails.configuration.FacebookAppId}&redirect_uri=#{redirect_url}&client_secret=#{Rails.configuration.FacebookAppSecret}&code=#{code}"

    request = Typhoeus::Request.new(auth_url, :method => :get, :timeout => 10000)

    hydra = Typhoeus::Hydra.new
    hydra.queue(request)
    hydra.run
    response = request.response

    if response.code == "400"
      { :success => false }
    else
      access_token = response.body.split("=")[1]
      me_url = "https://graph.facebook.com/me?access_token=#{access_token}"
      request = Typhoeus::Request.new(me_url, :method => :get, :timeout => 10000)
      hydra.queue(request)
      hydra.run
      response = request.response

      { :success => true, :me => ActiveSupport::JSON.decode(response.body)}
    end
  end
end