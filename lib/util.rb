class Util
  def self.errors_with_markup( errors )
    if errors.is_a?( String )
      errors
    elsif errors.size == 1
      errors.full_messages.to_s
    else
      "<ol style='margin-bottom: 0px;'>" + errors.full_messages.collect{ | err | "<li>" + err + "</li>" }.to_s + "</ol>"
    end
  end

  def self.strip_html(str)
    str = str.gsub(/<\/?[^>]*>/, "")
    str
  end

  def self.extract_urls(str)
    str.scan(/(?:http:\/\/|https:\/\/|www\.)[a-z0-9]+(?:[\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(?:(?::[0-9]{1,5})?\/[^\s]*)?/ix)
  end

  def self.valid_url?(url)
    !url.match(/(?:http:\/\/|https:\/\/|www\.)[a-z0-9]+(?:[\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(?:(?::[0-9]{1,5})?\/[^\s]*)?/ix).nil?
  end

  def self.get_slug(name)
    # strip the string
    ret = name.strip

    #blow away apostrophes
    ret.gsub! /['`]/, ""

    # @ --> at, and & --> and
    ret.gsub! /\s*@\s*/, " at "
    ret.gsub! /\s*&\s*/, " and "

    # replace all non alphanumeric, periods with dash
    ret.gsub! /\s*[^A-Za-z0-9\.]\s*/, '-'

    # replace underscore with dash
    ret.gsub! /[-_]{2,}/, '-'

    # convert double dashes to single
    ret.gsub! /-+/, "-"

    # strip off leading/trailing dash
    ret.gsub! /\A[-\.]+|[-\.]+\z/, ""

    ret.downcase
  end
end