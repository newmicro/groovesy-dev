require "spec_helper"

describe WallController do
  context "user is logged in" do
    before(:each) do
      @user = User.create({:first_name => "Blake", :last_name => "Miller", :email => "blakeleemiller@gmail.com", :password => "test", :password_confirmation => "test" })
      session[:user_id] = @user.id
    end

    describe " .new" do
      it "should render the new template" do
        get :new
        response.should render_template("new")
      end

      it "should instantiate a new wall instance" do
        get :new
        assigns[:wall].should_not be nil
      end
    end

    describe " .create" do
      it "should create the new wall when the form is valid" do
        wall = mock_model(Wall)
        posts = mock(Array)

        post_params = { :wall => { "name" => "test one", "description" => "Some long description" } }
        Wall.should_receive(:new).with(post_params[:wall]).and_return(wall)
        wall.should_receive(:set_slug)
        wall.should_receive(:valid?).and_return(true)
        wall.should_receive(:admin_id=).with(@user.id)
        wall.should_receive(:save)
        wall.should_receive(:posts).and_return(posts)
        posts.should_receive(:all).and_return([])

        post :create, post_params
        wall.should_not be nil
        response.should render_template("wall/view")
      end

      it "should not create the wall when the form is invalid" do
        wall = mock_model(Wall)

        post_params = { :wall => { "name" => nil, "description" => "Some long description" } }
        Wall.should_receive(:new).with(post_params[:wall]).and_return(wall)
        wall.should_receive(:set_slug)
        wall.should_receive(:valid?).and_return(false)
        wall.should_receive(:admin_id=).with(@user.id)

        post :create, post_params
        flash.now[:error].should_not be_nil
        response.should render_template("wall/new")
      end
    end

    describe " .update" do
      it "should update the wall when the form is valid" do
        wall = Wall.create(:name => "test one", :slug => "test-one", :admin_id => @user.id)
        post_params = { :id => wall.id, :wall => { "name" => "test two", "description" => "Some long description" } }
        put :update, post_params
        assigns[:wall].valid?.should be true
        response.should redirect_to(view_wall_url(assigns[:wall].slug))
      end

      it "should not update the wall and re-render the form when the wall is invalid" do
        wall = Wall.create(:name => "test one", :slug => "test-one", :admin_id => @user.id)
        post_params = { :id => wall.id, :wall => { "name" => "", "description" => "Some long description" } }
        put :update, post_params
        assigns[:wall].valid?.should be false
        response.should render_template("wall/edit")
      end
    end

    describe " .view" do
      it "should find the wall from the page slug and get it's posts" do
        wall = Wall.create(:name => "test one", :slug => "test-one", :admin_id => @user.id)
        wp = WallPost.create(:poster_user_id => @user.id, :post_text => "hello", :wall_id => wall.id, :post_type => "public_wall")
        get :view, { :slug => "test-one" }
        assigns[:wall].should_not be nil
        assigns[:wall].name.should == wall.name
        assigns[:posts].size.should be == 1
        assigns[:posts].first.post_text.should == "hello"
        response.should render_template("wall/view")
      end
    end

    describe ".destroy" do
      it "should find the wall and delete its posts" do
        wall_mock = mock_model(Wall)
        Wall.should_receive(:find).with(wall_mock.id).and_return(wall_mock)
        posts = mock(Array)
        wall_members = mock(Array)
        wall_mock.should_receive(:posts).and_return(posts)
        wall_mock.should_receive(:destroy)
        wall_mock.should_receive(:wall_members).and_return(wall_members)
        posts.should_receive(:each)
        wall_members.should_receive(:each)
        delete :destroy, { :id => wall_mock.id }
        response.should render_template("wall/my_walls")
      end
    end

    describe ".my_walls" do
      it "should get a list of walls for the logged in user" do
        wall = Wall.create(:name => "test one", :slug => "test-one", :admin_id => @user.id)
        get :my_walls
        assigns[:walls].size.should be == 1
        assigns[:walls].first.name.should == "test one"
      end
    end

    describe " .ajax_public_post" do
      it "should create a post and render the ajax template when the post is valid" do
        wall = Wall.create(:name => "test one", :slug => "test-one", :admin_id => @user.id)
        params = { :wall_post => { :poster_user_id => @user.id, :post_text => "asdf", :wall_id => wall.id, :post_type => "public_wall" } }
        xhr :post, :ajax_public_post, params
        wp = WallPost.find_by_post_text("asdf")
        wp.should_not be nil
        wp.post_type.should == "public_wall"
        response.should render_template("wall/ajax/public/ajax_post")
      end

      it "should not create a post when the post is invalid" do
        wall = Wall.create(:name => "test one", :slug => "test-one", :admin_id => @user.id)
        params = { :wall_post => { :poster_user_id => @user.id, :post_text => "saxondale", :wall_id => wall.id, :post_type => "" } }
        xhr :post, :ajax_public_post, params
        WallPost.find_by_post_text("asdf").should be nil
        response.body.should be_empty
      end
    end

    describe " .ajax_comment" do
      before(:each) do
        @wall = Wall.create(:name => "test one", :slug => "test-one", :admin_id => @user.id)
        @post = WallPost.create(:poster_user_id => @user.id, :post_text => "keke", :wall_id => @wall.id, :post_type => "public_wall")
      end

      it "should create a comment and render the ajax user_wall template when the comment is valid and the context is user_wall" do
        params = { :id => @post.id, :context => "user_wall", :wall_post_comment => { :comment => "hello" } }
        xhr :post, :ajax_comment, params
        c = WallPostComment.find_by_comment("hello")
        c.should_not be nil
        c.commenter_id.should == @user.id
        response.should render_template("wall/ajax/ajax_comment")
      end

      it "should create a comment and render the ajax public_wall template when the comment is valid and the context is public_wall" do
        params = { :id => @post.id, :context => "public_wall", :wall_post_comment => { :comment => "hello" } }
        xhr :post, :ajax_comment, params
        c = WallPostComment.find_by_comment("hello")
        c.should_not be nil
        c.commenter_id.should == @user.id
        response.should render_template("wall/ajax/public/ajax_comment")
      end

      it "should NOT create a comment and should render nothing when the comment is invalid" do
        params = { :id => @post.id, :context => "user_wall", :wall_post_comment => { :comment => "" } }
        xhr :post, :ajax_comment, params
        WallPostComment.count.should == 0
        response.body.should be_empty
      end
    end

    describe ".ajax_load_comments" do
      before(:each) do
        @wall = Wall.create(:name => "test one", :slug => "test-one", :admin_id => @user.id)
        @post = WallPost.create(:poster_user_id => @user.id, :post_text => "keke", :wall_id => @wall.id, :post_type => "public_wall")
      end

      it "should load comments for the given post and page and render the right partial when the context is public_wall and there is a comment" do
        WallPostComment.create(:wall_post_id => @post.id, :commenter_id => @user.id, :comment => "hello")
        params = { :post_id => @post.id, :context => "public_wall", "page" => 0 }
        xhr :post, :ajax_load_comments, params
        assigns[:comments].size.should == 1
        assigns[:comments].first[:comment].should == "hello"
        response.should render_template("wall/ajax/public/ajax_load_comments")
      end

      it "shouldn't load any comments for the given post and page and render nothing if there are no comments" do
        params = { :post_id => @post.id, :context => "public_wall", "page" => 0 }
        xhr :post, :ajax_load_comments, params
        assigns[:comments].size.should == 0
        response.body.should be_empty
      end
    end

    describe ".ajax_delete_post" do
      before(:each) do
        @wall = Wall.create(:name => "test one", :slug => "test-one", :admin_id => @user.id)
        @post = WallPost.create(:poster_user_id => @user.id, :post_text => "keke", :wall_id => @wall.id, :post_type => "public_wall")
      end

      it "should call delete post and render the template if the post is found and belongs to the user" do
        post = mock_model(WallPost)
        WallPost.should_receive(:find).with(@post.id).and_return(post)
        post.should_receive(:delete_post)
        post.should_receive(:user).and_return(nil)
        post.should_receive(:poster_user).and_return(@user)
        params = { :id => @post.id }
        xhr :post, :ajax_delete_post, params
        response.should render_template("wall/ajax/ajax_delete_post")
        assigns[:post_id].should == post.id
      end

      it "should render nothing if the post is not found" do
        post = mock_model(WallPost)
        WallPost.should_receive(:find).with(@post.id).and_return(nil)
        params = { :id => @post.id }
        xhr :post, :ajax_delete_post, params
        response.body.should be_empty
      end
    end

    describe ".ajax_subscribe" do
      before(:each) do
        @wall = Wall.create(:name => "test one", :slug => "test-one", :admin_id => @user.id)
      end

      it "should subscribe you to a wall if you're not subscribed and render the ajax_subscribe template" do
        params = { :id => @wall.id }
        xhr :post, :ajax_subscribe, params
        assigns[:user].reload
        assigns[:user].member_of?(@wall).should == true
        response.should render_template("wall/ajax/ajax_subscribe")
      end

      it "should render nothing if the user is already a member of the wall" do
        @user.join_wall(@wall)
        params = { :id => @wall.id }
        xhr :post, :ajax_subscribe, params
        assigns[:user].member_of?(@wall).should == true
        response.body.should be_empty
      end
    end

    describe ".ajax_unsubscribe" do
      before(:each) do
        @wall = Wall.create(:name => "test one", :slug => "test-one", :admin_id => @user.id)
      end

      it "should unsubscribe you from a wall if you're subscribed and render the ajax_unsubscribe template" do
        params = { :id => @wall.id }
        @user.join_wall(@wall)
        xhr :post, :ajax_unsubscribe, params
        assigns[:user].reload
        assigns[:user].member_of?(@wall).should == false
        response.should render_template("wall/ajax/ajax_unsubscribe")
      end

      it "should render nothing if the user isn't a member of the wall" do
        params = { :id => @wall.id }
        xhr :post, :ajax_unsubscribe, params
        response.body.should be_empty
      end
    end

    describe ".unsubscribe" do
      before(:each) do
        @wall = Wall.create(:name => "test one", :slug => "test-one", :admin_id => @user.id)
      end

      it "should unsubscribe you from a wall and then render the my_walls template" do
        params = { :id => @wall.id }
        @user.join_wall(@wall)
        post :unsubscribe, params
        assigns[:user].reload
        assigns[:user].member_of?(@wall).should == false
        response.should render_template("wall/my_wall")
      end
    end
  end
end