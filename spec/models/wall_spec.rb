require "spec_helper"

describe Wall do
  context "validation" do
    before(:each) do
      @user = User.create(:first_name => "Blake",
                          :last_name => "Miller",
                          :email => "blakeleemiller@gmail.com",
                          :password => "test",
                          :password_confirmation => "test")
    end

    it "should validate presence of name" do
      w = Wall.new(:description => "Hello", :admin_id => @user.id, :slug => "test")
      w.valid?
      w.should have(1).error_on(:name)
      w.errors.on(:name).should == "is required"
    end

    it "should validate uniqueness of name" do
      w1 = Wall.create(:name => "test1", :description => "Hello", :admin_id => @user.id, :slug => "test1")
      w2 = Wall.new(:name => w1.name, :description => w1.description, :admin_id => w1.admin_id, :slug => w1.slug)
      w2.valid?
      w2.should have(1).error_on(:name)
      w2.errors.on(:name).should == "is already taken"
    end

    it "should validate presence of an admin" do
      w = Wall.new(:name => "test1", :description => "Hello", :admin_id => nil, :slug => "test1")
      w.valid?
      w.should have(1).error_on(:admin_id)
      w.errors.on(:admin_id).should == "is required"
    end

    it "should validate presence of a slug" do
      w = Wall.new(:name => "test1", :description => "Hello", :admin_id => @user.id, :slug => nil)
      w.valid?
      w.should have(1).error_on(:slug)
      w.errors.on(:slug).should == "is required"
    end

    it "should validate the length of tags" do
      tag = ""
      (0..256).each{ | x | tag += "t"}
      w = Wall.new(:name => "test1", :description => "hello", :admin_id => @user.id, :slug => "test1", :tags => tag )
      w.valid?
      w.should have(1).error_on(:tags)
      w.errors.on(:tags).should == "is too long (maximum is 255 characters)"
    end

    it "should validate the length of name" do
      name = ""
      (0..256).each{ | x | name += "t"}
      w = Wall.new(:name => name, :description => "hello", :admin_id => @user.id, :slug => "test1", :tags => "asdf" )
      w.valid?
      w.should have(1).error_on(:name)
      w.errors.on(:name).should == "is too long (maximum is 255 characters)"
    end
  end
end