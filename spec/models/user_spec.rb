require "spec_helper"

describe User do
  describe ".search_friends" do
    before(:each) do
      @user = User.create({:first_name => "Blake", :last_name => "Miller", :email => "blakeleemiller@gmail.com", :password => "test", :password_confirmation => "test" })
      @friend1 = User.create({:first_name => "Noel", :last_name => "Geren", :email => "ngeren@gmail.com", :password => "test", :password_confirmation => "test" })
    end

    it "should include a user's friends" do
      FriendUser.create({:user_id => @user.id, :friend_user_id => @friend1.id, :status => "confirmed"})
      friends = @user.search_friends("Noel")
      friends.first.should == @friend1
    end

    it "should include a user's inverse_friends" do

    end
  end

  describe ".member_of?" do
    it "should return true if the user is a member of a wall" do
      u = User.create(:first_name => "Blake", :last_name => "Miller", :email => "blakeleemiller@gmail.com", :password => "test", :password_confirmation => "test")
      wall = Wall.create(:name => "test1", :description => "Hello", :admin_id => u.id, :slug => "test1")
      WallMember.create(:wall_id => wall.id, :user_id => u.id)
      u.member_of?(wall).should == true
    end
  end

  describe ".join_wall" do
    it "should join a wall if the user isn't already a member" do
      u = User.create(:first_name => "Blake", :last_name => "Miller", :email => "blakeleemiller@gmail.com", :password => "test", :password_confirmation => "test")
      wall = Wall.create(:name => "test1", :description => "Hello", :admin_id => u.id, :slug => "test1")
      u.join_wall(wall)
      u.reload
      u.member_of?(wall).should == true
    end
  end

  describe ".leave_wall" do
    it "should leave a wall if the user is already a member" do
      u = User.create(:first_name => "Blake", :last_name => "Miller", :email => "blakeleemiller@gmail.com", :password => "test", :password_confirmation => "test")
      wall = Wall.create(:name => "test1", :description => "Hello", :admin_id => u.id, :slug => "test1")
      u.join_wall(wall)
      u.leave_wall(wall)
      u.member_of?(wall).should == false
    end
  end
end