require "spec_helper"

describe WallMember do
  context "validation" do
    before(:each) do
      @user = User.create(:first_name => "Blake",
                          :last_name => "Miller",
                          :email => "blakeleemiller@gmail.com",
                          :password => "test",
                          :password_confirmation => "test")
      @wall = Wall.create(:name => "test", :slug => "test")
    end

    it "should validate presence of wall" do
      wm = WallMember.new(:wall_id => nil, :user_id => @user.id)
      wm.valid?
      wm.should have(1).error_on(:wall_id)
      wm.errors.on(:wall_id).should == "is required"
    end

    it "should validate presence of user" do
      wm = WallMember.new(:wall_id => @wall.id, :user_id => nil)
      wm.valid?
      wm.should have(1).error_on(:user_id)
      wm.errors.on(:user_id).should == "is required"
    end
  end
end