require "rvm/capistrano"
require "bundler/capistrano"

set :application, "notespin"  # CHANGE THIS LINE TO USE YOUR WEBSITE'S NAME
set :repository,  "git@github.com:blakeage/Note-Spin.git"
set :scm, :git

role :web, "50.56.188.150"
role :app, "50.56.188.150"
role :db, "50.56.188.150", :primary => true

set :deploy_to, "/home/notespin"  # CHANGE THIS LINE TO POINT TO THE CORRECT PATH
set :user, "notespin"
set :password, "j@kd8(Djsk"
set :use_sudo, false
set :branch, "master"
set :deploy_via, :remote_cache
set :normalize_asset_timestamps, false
set :rvm_bin_path, "/usr/local/rvm/bin"
set :rvm_path, "/usr/local/rvm"
set :rvm_ruby_string, "ree-1.8.7-2011.12@default"

# =============================================================================
# TASKS
# =============================================================================

namespace :customs do
  task :symlink, :roles => :app do
    run "ln -nfs #{File.join(shared_path, 'uploads')} #{File.join(release_path,'public','images','uploads')}"
  end
end

namespace :deploy do
   task :start do ; end
   task :stop do ; end
   task :restart, :roles => :app, :except => { :no_release => true } do
     run "cd #{current_path} && bundle exec rake assets:clean && bundle exec rake assets:precompile"
     run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
   end
end

after("deploy:create_symlink", "customs:symlink")
after "deploy", "deploy:cleanup"