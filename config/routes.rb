Earful::Application.routes.draw do
  # The priority is based upon order of creation:
  # first created -> highest priority.

  root :to => "content#index"

  match '/wall/ajax_load_post_details' => "wall#ajax_load_post_details"
  match '/wall/public/ajax_subscribe/:id' => "wall#ajax_subscribe", :as => :wall_ajax_subscribe
  match '/wall/public/ajax_unsubscribe/:id' => "wall#ajax_unsubscribe", :as => :wall_ajax_unsubscribe
  match '/wall/unsubscribe/:id' => "wall#unsubscribe", :as => :wall_unsubscribe
  match '/wall/search/:tag' => "wall#search", :as => :wall_search
  match '/wall/public/ajax_post' => "wall#ajax_public_post", :as => :wall_ajax_public_post
  match '/wall/ajax_post' => "wall#ajax_post", :as => :wall_ajax_post

  match '/streams/ajax_post' => "streams#ajax_post", :as => :streams_ajax_post
  match '/streams/pin_user' => "streams#pin_user", :as => :streams_pin_user
  match '/streams/ajax_unpin_user/:id' => "streams#ajax_unpin_user", :as => :streams_ajax_unpin_user
  match '/streams/ajax_load_posts' => "streams#ajax_load_posts", :as => :streams_ajax_load_posts
  match '/streams/like/:id' => "streams#ajax_like", :as => :streams_ajax_like
  match '/streams/repin/:id' => "streams#ajax_repin", :as => :streams_repin
  match '/streams/ajax_post_repin' => "streams#ajax_post_repin", :as => :streams_ajax_post_repin
  match '/streams/ajax_load_likes/:id' => "streams#ajax_load_likes", :as => :streams_ajax_load_likes
  match '/streams/comment/:id' => "streams#ajax_comment", :as => :streams_ajax_comment
  match '/streams/ajax_load_comments' => "streams#ajax_load_comments"
  match '/streams/ajax_delete_post/:id' => "streams#ajax_delete_post", :as => :streams_ajax_delete_post

  match '/content/ajax_load_comments' => "content#ajax_load_comments", :as => :content_ajax_load_comments
  match '/content/comment/:id' => "content#ajax_comment", :as => :content_ajax_comment
  match '/privacy-policy' => "content#privacy_policy"
  #match '/boo' => "content#boo"

  resources :users do
    member do
      get "wall", "fb_connect", "profile"
    end

    collection do
      get "ajax_find_friend", "invite"
      post "send_invitation", "fb_register"
    end
  end

  resources :posts

  resources :notifications do
    collection do
      get "ajax_load"
      post "ajax_ack"
    end
  end

  resources :login do
    collection do
      get "fb_login", "retrieve_password", "reset_password"
      post "mail_guid", "save_password"
    end
  end

  resources :wall, :except => :show do
    collection do
      get "my_walls", "friends", "my_likes"
    end
  end

  resources :streams, :except => :show

  match '/wall/:slug' => "wall#view", :as => :view_wall

  resources :friends do
    collection do
      get "find"
      post "search"
    end
  end

  match '/friend_requests/' => "friend_requests#index", :as => :friend_requests_index
  match '/friend_requests/update/:id/:status' => "friend_requests#update", :as => :friend_requests_update
  match '/friend_requests/new/:id' => "friend_requests#new", :as => :new_friend_requests
  match '/friend_requests/:id/ajax_new' => "friend_requests#ajax_new", :as => :ajax_new_friend_requests


  match '/email/unsubscribe/:key' => "email_unsubscribe#index", :as => :email_unsubscribe
  match '/facebook/invited' => "content#facebook_invited"
  match '/facebook' => "content#facebook_app"
end
